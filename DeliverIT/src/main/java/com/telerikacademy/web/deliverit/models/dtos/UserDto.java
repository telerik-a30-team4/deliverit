package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "Customer first name can't be empty")
    @Size(min = 3, max = 15, message = "First name should be between 3 and 15 symbols")
    private String firstName;

    @NotNull(message = "Customer last name can't be empty")
    @Size(min = 3, max = 15, message = "Last name should be between 3 and 15 symbols")
    private String lastName;

    @NotNull(message = "Email can't be empty")
    @Email(message = "Email should be valid")
    private String email;

    @NotNull
    @Size(min = 4, max = 20)
    private String password;

    @Positive
    @NotNull(message = "Customer address can't be empty")
    private int addressId;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

}
