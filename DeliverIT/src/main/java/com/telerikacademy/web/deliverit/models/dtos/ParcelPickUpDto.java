package com.telerikacademy.web.deliverit.models.dtos;

public class ParcelPickUpDto {

    private boolean pickUpFromWarehouse = true;

    public ParcelPickUpDto() {
    }

    public boolean isPickUpFromWarehouse() {
        return pickUpFromWarehouse;
    }

    public void setPickUpFromWarehouse(boolean pickUpFromWarehouse) {
        this.pickUpFromWarehouse = pickUpFromWarehouse;
    }
}
