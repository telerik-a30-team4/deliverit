package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.AdminFailureException;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.SearchTermDto;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/customers")
public class CustomerMvcController {

    private final CustomerService customerService;
    private final CountryService countryService;
    private final CityService cityService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerMvcController(CustomerService customerService,
                                 CountryService countryService,
                                 CityService cityService,
                                 AuthenticationHelper authenticationHelper) {
        this.customerService = customerService;
        this.countryService = countryService;
        this.cityService = cityService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @GetMapping
    public String showAllCustomers(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
        model.addAttribute("customers", customerService.getAll());
        model.addAttribute("searchTermDto", new SearchTermDto());
        return "customers";
    }

    @GetMapping("/{id}")
    public String showSingleCustomer(@PathVariable int id,
                                     Model model,
                                     HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if (id != user.getId() && !user.getRole().equals(Role.ADMIN)) {
                throw new AuthenticationFailureException("You cannot access other user info.");
            }
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            User customer = customerService.getById(id);
            model.addAttribute("customer", customer);
            return "customer";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/search")
    public String searchForCustomer(@ModelAttribute SearchTermDto searchTermDto,
                                    HttpSession session,
                                    Model model) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        if (searchTermDto.getSearchTerm().equals("")) searchTermDto.setSearchTerm(null);

        var search = customerService.search(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.ofNullable(searchTermDto.getSearchTerm()),
                Optional.ofNullable(searchTermDto.getSort()));

        model.addAttribute("customers", search);

        return "customers";
    }
}