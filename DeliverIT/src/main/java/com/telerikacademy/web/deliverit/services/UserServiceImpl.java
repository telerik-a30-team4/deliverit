package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.UserRepositoryImpl;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import com.telerikacademy.web.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public  User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

}
