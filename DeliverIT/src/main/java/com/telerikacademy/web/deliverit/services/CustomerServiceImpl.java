package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    public static final String NOT_THE_NEEDED_CUSTOMER_OR_ADMIN = "Only owner or admin can request or modify.";

    private final CustomerRepository customerRepository;
    private final UserRepository userRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, UserRepository userRepository) {
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return customerRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return customerRepository.getById(id);
    }

    @Override
    public void create(User user) {
        boolean isThereSuchUser = true;
        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            isThereSuchUser = false;
        }

        if (isThereSuchUser) {
            throw new DuplicateEntityException("User", "e-mail", user.getEmail());
        }

        user.setRole(Role.USER);
        customerRepository.create(user);
    }

    @Override
    public void update(User userToUpdate, User loggedUser) {
        if (userToUpdate.getId() != loggedUser.getId() && !loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }

        boolean isThereSuchUser = true;
        try {
            userRepository.getByEmail(userToUpdate.getEmail());
        } catch (EntityNotFoundException e) {
            isThereSuchUser = false;
        }

        if (isThereSuchUser) {
            throw new DuplicateEntityException("User", "e-mail", userToUpdate.getEmail());
        }

        customerRepository.update(userToUpdate);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getId() != id && !user.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }
        customerRepository.delete(id);
    }

    //todo only admins
    @Override
    public List<User> search(Optional<String> email,
                             Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> keyWord,
                             Optional<String> sort) {
        return customerRepository.search(email, firstName, lastName, keyWord, sort);
    }

    @Override
    public List<Parcel> showParcelsByCustomerId(int customerId, User user) {
        if (customerId != user.getId() && !user.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }
        return customerRepository.showParcelsByCustomerId(customerId);
    }
}