package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Category;

import java.util.List;

public interface CategoryRepository {
    List<Category> getAll();

    Category getById(int id);
}
