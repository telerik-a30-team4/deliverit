package com.telerikacademy.web.deliverit.models.dtos;


import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterUserDto extends LoginDto {

    /*@Valid
    private UserDto userDto;*/

    @NotNull(message = "Customer first name can't be empty")
    @Size(min = 3, max = 15, message = "First name should be between 3 and 15 symbols")
    private String firstName;

    @NotNull(message = "Customer last name can't be empty")
    @Size(min = 3, max = 15, message = "Last name should be between 3 and 15 symbols")
    private String lastName;

   /* @NotNull(message = "Email can't be empty")
    @Email(message = "Email should be valid")
    private String email;
*/
    @NotNull
    @Size(min = 4, max = 20)
    private String passwordConfirm;

    @Valid
    private AddressDto addressDto;

    public RegisterUserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

 /*   @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }*/

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public AddressDto getAddressDto() {
        return addressDto;
    }

    public void setAddressDto(AddressDto addressDto) {
        this.addressDto = addressDto;
    }
}
