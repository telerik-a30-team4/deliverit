package com.telerikacademy.web.deliverit.models.enums;

public enum Role {
    ADMIN,
    USER
}
