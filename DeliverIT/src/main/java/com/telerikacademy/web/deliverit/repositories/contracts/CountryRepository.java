package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();
}
