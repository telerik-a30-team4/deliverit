package com.telerikacademy.web.deliverit.models.dtos;

public class SearchTermDto {

    private String searchTerm;
    private String sort;

    public SearchTermDto() {
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {

        this.searchTerm = searchTerm;
    }
}