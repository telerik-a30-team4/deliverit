package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.repositories.contracts.CountryRepository;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.getAll();
    }

}
