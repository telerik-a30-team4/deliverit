package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    public static final String ADMIN_ERROR_MESSAGE = "Only admins can create, update or delete shipments.";


    private final ShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }

    @Override
    public List<Shipment> getAll() {
        return shipmentRepository.getAll();
    }

    @Override
    public Shipment getById(int id) {
        return shipmentRepository.getById(id);
    }

    @Override
    public void create(Shipment shipment, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_ERROR_MESSAGE);
        }

        if (shipment.getArrivalWarehouse().getId() == shipment.getDepartureWarehouse().getId()) {
            throw new UnsupportedOperationException("Arrival and departure warehouse must be different");
        }

        if (shipment.getArrivalDate() != null &&
                shipment.getDepartureDate() != null &&
                shipment.getArrivalDate().isBefore(shipment.getDepartureDate())) {
            throw new UnsupportedOperationException("Departure date must be before arrival date.");
        }

        shipment.setEmployee(loggedUser);
        shipmentRepository.create(shipment);
    }

    @Override
    public void update(Shipment shipment, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_ERROR_MESSAGE);
        }

        if (shipment.getArrivalWarehouse().getId() == shipment.getDepartureWarehouse().getId()) {
            throw new UnsupportedOperationException("Arrival and departure warehouse must be different");
        }
        if (shipment.getArrivalDate() != null &&
                shipment.getDepartureDate() != null &&
                shipment.getArrivalDate().isBefore(shipment.getDepartureDate())) {
            throw new UnsupportedOperationException("Departure date must be before arrival date.");
        }
        if (shipment.getDepartureDate() != null) {
            shipment.setStatus(Status.ON_THE_WAY);
        }
        if (shipment.getArrivalDate() != null) {
            shipment.setStatus(Status.COMPLETED);
        }
        shipmentRepository.update(shipment);
    }

    @Override
    public void delete(int id, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_ERROR_MESSAGE);
        }
        if (!shipmentRepository.getShipmentParcels(id).isEmpty()) {
            throw new EntityNotFoundException("There are parcels in this shipment, cannot be deleted.");
        }
        shipmentRepository.delete(id);
    }

    @Override
    public List<Shipment> filter(Optional<Integer> warehouseId,
                                 Optional<String> destination,
                                 Optional<Integer> customerId,
                                 Optional<String> employeeKeyWord,
                                 Optional<Integer> statusId,
                                 Optional<String> sort) {
        return shipmentRepository.filter(warehouseId, destination, customerId, employeeKeyWord, statusId, sort);
    }

    @Override
    public List<Parcel> getShipmentParcels(int id) {
        return shipmentRepository.getShipmentParcels(id);
    }

    @Override
    public void addParcel(Shipment shipment, Parcel parcel, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_ERROR_MESSAGE);
        }
        shipmentRepository.addParcel(shipment, parcel);
    }

    @Override
    public List<Shipment> search(String searchTerm) {
        return shipmentRepository.search(searchTerm);
    }


}
