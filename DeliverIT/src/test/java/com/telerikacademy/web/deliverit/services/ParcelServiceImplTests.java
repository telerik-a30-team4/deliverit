package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceImplTests {

    @Mock
    ParcelRepository parcelRepository;

    @InjectMocks
    ParcelServiceImpl parcelService;

    @Mock
    ShipmentRepository shipmentRepository;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(parcelRepository.getAll())
                .thenReturn(new ArrayList<>());

        parcelService.getAll();

        Mockito.verify(parcelRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    void getById_should_callRepository() {
        Parcel parcel = createMockParcel();
        User admin = createMockAdmin();

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Parcel result = parcelService.getById(parcel.getId(), admin);

        Assertions.assertAll(
                () -> Assertions.assertEquals(parcel.getId(), result.getId()),
                () -> Assertions.assertEquals(parcel.getCategory(), result.getCategory()),
                () -> Assertions.assertEquals(parcel.getCustomer(), result.getCustomer()),
                () -> Assertions.assertEquals(parcel.getWeight(), result.getWeight())
        );
    }

    @Test
    void getById_should_throw_when_loggedUserNotAdminOrCreator() {
        Parcel parcel = createMockParcel();
        User someone = createMockUser();
        someone.setId(2);

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.getById(parcel.getId(), someone));
    }

    @Test
    void create_should_callRepository_when_userIsLogged() {
        Parcel parcel = createMockParcel();
        User user = createMockUser();

        parcelService.create(parcel, user);

        Mockito.verify(parcelRepository,
                Mockito.times(1)).create(parcel);
    }

    @Test
    void create_should_throw_when_loggedIsNotUser() {
        Parcel parcel = createMockParcel();
        User user = createMockAdmin();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.create(parcel, user));
    }

    @Test
    void delete_should_callRepository_when_adminIsLogged() {
        Parcel parcel = createMockParcel();
        User admin = createMockAdmin();

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        parcelService.delete(parcel.getId(), admin);

        Mockito.verify(parcelRepository,
                Mockito.times(1)).delete(parcel.getId());
    }

    @Test
    void delete_should_throw_when_creatorNotLogged() {
        Parcel parcel = createMockParcel();
        User user = createMockUser();
        user.setId(2);

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.delete(parcel.getId(), user));
    }

    @Test
    void filter_should_callRepository() {
        parcelService.filter(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        Mockito.verify(parcelRepository,
                Mockito.times(1)).filter(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @Test
    void sort_should_callRepository() {
        parcelService.sort(Optional.empty(), Optional.empty());

        Mockito.verify(parcelRepository,
                Mockito.times(1)).sort(Optional.empty(), Optional.empty());
    }

    @Test
    void getShipmentByParcelId_should_callRepository() {
        Parcel parcel = createMockParcel();
        Shipment shipment = createMockShipment();
        User admin = createMockAdmin();

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Mockito.when(parcelRepository.getShipmentByParcelId(parcel.getId()))
                .thenReturn(shipment);

        Shipment result = parcelService.getShipmentByParcelId(parcel.getId(), admin);

        Assertions.assertAll(
                () -> Assertions.assertEquals(shipment.getId(), result.getId()),
                () -> Assertions.assertEquals(shipment.getArrivalWarehouse(), result.getArrivalWarehouse()),
                () -> Assertions.assertEquals(shipment.getDepartureWarehouse(), result.getDepartureWarehouse())
        );
    }

    @Test
    void getShipmentByParcelID_should_throw_when_creatorIsNotLogged() {
        Parcel parcel = createMockParcel();
        User user = createMockUser();
        user.setId(2);

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.getShipmentByParcelId(parcel.getId(), user));


    }

    @Test
    void update_should_callRepository() {
        Parcel parcel = createMockParcel();
        User admin = createMockAdmin();
        Shipment shipment = createMockShipment();

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Mockito.when(parcelRepository.getShipmentByParcelId(parcel.getId()))
                .thenReturn(shipment);

        parcelService.update(parcel, admin);

        Mockito.verify(parcelRepository,
                Mockito.times(1)).update(parcel);
    }

    @Test
    void update_should_throw_when_creatorNotLogged() {
        Parcel parcel = createMockParcel();
        User user = createMockUser();
        user.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.update(parcel, user));

    }

    @Test
    void update_should_throw_when_shipmentIsCopleted() {
        Parcel parcel = createMockParcel();
        User admin = createMockAdmin();
        Shipment shipment = createMockShipment();
        shipment.setStatus(Status.COMPLETED);

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Mockito.when(parcelRepository.getShipmentByParcelId(parcel.getId()))
                .thenReturn(shipment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.update(parcel, admin));
    }

    @Test
    void update_should_catch_callRepository_when_thereIsNoShipment() {
        Parcel parcel = createMockParcel();
        User admin = createMockAdmin();

        Mockito.when(parcelRepository.getById(parcel.getId()))
                .thenReturn(parcel);

        Mockito.when(parcelRepository.getShipmentByParcelId(parcel.getId()))
                .thenThrow(EntityNotFoundException.class);

        parcelService.update(parcel, admin);

        Mockito.verify(parcelRepository,
                Mockito.times(1)).update(parcel);
    }


    @Test
    public void removeParcelFromShipment_should_callRepository_whenUserIsAdminAndParcelIsInShipment() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockAdmin();
        Parcel mockParcel = createMockParcel();

        Mockito.when(parcelRepository.getShipmentByParcelId(mockParcel.getId()))
                .thenReturn(mockShipment);

        parcelService.removeParcelFromShipment(mockParcel, mockUser);

        Mockito.verify(parcelRepository,
                Mockito.times(1)).removeParcelFromShipment(mockParcel);
    }

//    @Test
//    public void removeParcelFromShipment_should_throwException_whenUserIsNotAdminAndParcelIsInShipment() {
//        Shipment mockShipment = createMockShipment();
//        User mockUser = createMockUser();
//        Parcel mockParcel = createMockParcel();
//
//        Set<Parcel> mockListOfParcels = new HashSet<>();
//        mockListOfParcels.add(mockParcel);
//
////        mockShipment.setListOfParcels(mockListOfParcels);
//
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> parcelService.removeParcelFromShipment(mockParcel, mockUser));
//    }

    @Test
    public void removeParcelFromShipment_should_throwException_whenShipmentIsNotPreparing() {
        Shipment mockShipment = createMockShipment();
        mockShipment.setStatus(Status.COMPLETED);
        User mockAdmin = createMockAdmin();
        Parcel mockParcel = createMockParcel();

        Mockito.when(parcelRepository.getShipmentByParcelId(mockParcel.getId()))
                .thenReturn(mockShipment);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.removeParcelFromShipment(mockParcel, mockAdmin));
    }

}
