package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<User> getAll();

    User getById(int id);

    void create(User user);

    void update(User loggedUser, User userToUpdate);

    void delete(int id, User user);

    List<User> search(Optional<String> email,
                      Optional<String> firstName,
                      Optional<String> lastName,
                      Optional<String> keyWord,
                      Optional<String> sort);

    List<Parcel> showParcelsByCustomerId(int customerId, User user);
}
