package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();
}
