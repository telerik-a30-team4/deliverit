package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository {
    List<User> getAll();

    User getById(int id);

    void create(User user);

    void update(User user);

    void delete(int id);

/*    List<Customer> filterByEmail(String email);

    List<Customer> filterByName(Optional<String> firstName, Optional<String> lastName);

    List<Parcel> filterByIncomingParcels(int id);*/

    List<User> search(Optional<String> email,
                      Optional<String> firstName,
                      Optional<String> lastName,
                      Optional<String> keyWord,
                      Optional<String> sort);

    List<Parcel> showParcelsByCustomerId(int customerId);
}
