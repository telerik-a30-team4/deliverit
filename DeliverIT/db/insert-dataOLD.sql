INSERT INTO `countries` (`country_id`, `country_name`) VALUES
                                                           (1, 'Bulgaria'),
                                                           (4, 'Germany'),
                                                           (2, 'Greece'),
                                                           (5, 'Italy'),
                                                           (6, 'Spain'),
                                                           (3, 'Turkey'),
                                                           (7, 'United Kingdom');


INSERT INTO `cities` (`city_id`, `city_name`, `country_id`) VALUES
                                                                (1, 'Sofia', 1),
                                                                (2, 'Athens', 2),
                                                                (3, 'Ankara', 3),
                                                                (4, 'Varna', 1),
                                                                (5, 'Thessaloniki', 2),
                                                                (6, 'Istanbul', 3),
                                                                (7, 'Berlin', 4),
                                                                (8, 'Frankfurt', 4),
                                                                (9, 'Rome', 5),
                                                                (10, 'Torino', 5),
                                                                (11, 'Barcelona', 6),
                                                                (12, 'Madrid', 6),
                                                                (13, 'Manchester', 7),
                                                                (14, 'London', 7);


INSERT INTO `addresses` (`address_id`, `street`, `city_id`) VALUES
	(1, 'bul Tsarigradsko shose 80', 1),
	(2, 'bul Bulgaria 60', 1),
	(3, 'bul Hristo Botev 101', 4),
	(4, 'bul Vasil Levski 18', 4),
	(5, 'bul Athinon 8', 2),
	(6, 'bul Poseidonos 185', 2),
	(7, 'bul Stathmou 38', 5),
	(8, 'bul Papandreou 97', 5),
	(9, 'bul Dumlupinar 54', 3),
	(10, 'bul Mevlana 176', 3),
	(11, 'bul Dereboyu 11', 6),
	(12, 'bul Istanbul Chevre Yolu 44', 6),
	(13, 'Bundesstrasse 96a', 7),
	(14, 'str. Rudover 44', 7),
	(15, 'str. Mannheimer 2b', 8),
	(16, 'str. Bertha-von-Suttner 96', 8),
	(17, 'via di San Gregorio 156', 9),
	(18, 'viale Marco Polo', 9),
	(19, 'corso Giulio Cesare 133', 10),
	(20, 'via Po 56', 10),
	(21, 'Pg. de Sant Joan 29', 11),
	(22, 'La Rambla 11', 11),
	(23, 'Calle de Palo de Rosa 3a', 12),
	(24, 'Av. Juan Carlos I 85c', 12),
	(25, 'Princess Rd. 156', 13),
	(26, 'Oldfield Rd. 201', 13),
	(27, 'Victoria str. 15', 14),
	(28, 'Cromwell Rd. 111A', 14),
	(29, 'ul. Ohridsko ezero 18', 1),
	(30, 'ul. Cherno more 32', 4);


INSERT INTO `categories` (`category_id`, `category_name`) VALUES
	(1, 'Electronics'),
	(3, 'Fashion'),
	(4, 'Health_and_Beauty'),
	(2, 'Hobby'),
	(5, 'Home_and_Garden'),
	(7, 'Kids'),
	(6, 'Sports');


INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `e_mail`, `role_id`, `address_id`) VALUES
                                                                                                  (1, 'Dimitar', 'Bonchev', 'dimitar.bonchev@deliverit.com', 0, 4),
                                                                                                  (2, 'Dimo', 'Sotirov', 'dimo.sotirov@deliverit.com', 0, 9),
                                                                                                  (3, 'Petar', 'Georgiev', 'p.georgiev@deliverit.com', 0, 25),
                                                                                                  (4, 'Ivan', 'Petrov', 'ivan.petrov@deliverit.com', 0, 17),
                                                                                                  (5, 'Georgi', 'Georgiev', 'georgi.georgiev@deliverit.com', 0, 14),
                                                                                                  (6, 'Petar', 'Raykov', 'p.raykov@telerik.com', 1, 2),
                                                                                                  (7, 'Vladimir', 'Venkov', 'vladimir.venkov@gmail.com', 1, 6),
                                                                                                  (8, 'Todor', 'Andonov', 'todor.andonov@abv.bg', 1, 16),
                                                                                                  (9, 'Anastasia', 'Traycheva', 'a.traycheva@poshta.bg', 1, 23),
                                                                                                  (10, 'Milena', 'Marinova', 'm.mainova@gmail.com', 1, 12),
                                                                                                  (11, 'Iva', 'Atanasova', 'iva.atanasova99@abv.bg', 1, 20),
                                                                                                  (12, 'Elitsa', 'Krysteva', 'elitsa.krysteva9@gmail.com', 1, 8),
                                                                                                  (13, 'Maya', 'Konstantinova', 'konstantinova.maya@deliverit.com', 0, 1),
                                                                                                  (14, 'Kamen', 'Strahilov', 'kamen.s.@deliverit.com', 0, 1),
                                                                                                  (15, 'Hristo', 'Kolev', 'kolev-hristo@abv.bg', 1, 22),
                                                                                                  (16, 'Boris', 'Filipov', 'boris2000@gbg.bg', 1, 28),
                                                                                                  (17, 'Bojidar', 'Petrov', 'bojkata@poshta.bg', 1, 29),
                                                                                                  (18, 'Ivan', 'Dimitrov', 'vankata95@mail.bg', 1, 30),
                                                                                                  (19, 'Test user', 'test last name', 'test@fshja.com', 0, 10),
                                                                                                  (21, 'Test user2', 'test last name', 'test2@fshja.com', 0, 5),
                                                                                                  (22, 'Test user2', 'test last name', 'test3@fshja.com', 0, 1);


INSERT INTO `parcels` (`parcel_id`, `weight`, `category_id`, `customer_id`, `pick_up_from_warehouse`) VALUES
	(1, 2.1, 7, 6, 0),
	(2, 1.6, 4, 6, 0),
	(3, 14, 3, 6, 0),
	(4, 8.5, 6, 4, 1),
	(5, 6.3, 1, 4, 1),
	(6, 0.5, 2, 4, 1),
	(7, 23.5, 6, 4, 1),
	(8, 3.6, 1, 5, 0),
	(9, 2.7, 4, 6, 1),
	(10, 32, 6, 6, 1),
	(11, 11.2, 7, 6, 1),
	(12, 0.7, 5, 7, 1),
	(13, 9.8, 3, 7, 1),
	(14, 5.5, 2, 8, 1),
	(15, 2.1, 1, 8, 1),
	(16, 0.2, 1, 8, 1),
	(17, 14.7, 6, 9, 0),
	(18, 4.9, 1, 10, 0),
	(19, 2.6, 5, 10, 0),
	(20, 3.3, 4, 11, 0),
	(21, 7.9, 5, 11, 0),
	(22, 21, 2, 12, 1),
	(23, 17.9, 5, 12, 1),
	(24, 1.8, 1, 12, 1),
	(25, 2.4, 2, 13, 0),
	(26, 5.5, 4, 13, 0);


INSERT INTO `warehouses` (`warehouse_id`, `address_id`) VALUES
                                                            (1, 1),
                                                            (4, 3),
                                                            (2, 5),
                                                            (5, 7),
                                                            (3, 10),
                                                            (6, 11),
                                                            (7, 13),
                                                            (8, 15),
                                                            (9, 18),
                                                            (10, 19),
                                                            (11, 21),
                                                            (12, 24),
                                                            (13, 26),
                                                            (14, 27);


INSERT INTO `shipments` (`shipment_id`, `departure_date`, `arrival_date`, `status_id`, `employee_id`, `departure_warehouse_id`, `arrival_warehouse_id`) VALUES
	(1, '2021-08-11', NULL, 1, 5, 7, 1),
	(2, NULL, NULL, 0, 7, 12, 4),
	(3, '2021-08-05', '2021-08-09', 2, 3, 14, 7),
	(4, '2025-01-01', '2025-02-01', 2, 1, 1, 3),
	(5, '2021-08-06', NULL, 1, 2, 2, 7),
	(6, '2021-07-13', '2021-07-15', 2, 1, 3, 7);


INSERT INTO `order_details` (`order_detail_id`, `shipment_id`, `parcel_id`) VALUES
                                                                                (7, 1, 1),
                                                                                (8, 1, 2),
                                                                                (9, 1, 3),
                                                                                (10, 2, 4),
                                                                                (11, 2, 5),
                                                                                (12, 2, 6),
                                                                                (13, 2, 7),
                                                                                (14, 3, 8);
