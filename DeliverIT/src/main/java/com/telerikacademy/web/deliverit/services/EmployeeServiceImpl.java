package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    public static final String ONLY_ADMIN_ERROR_MESSAGE = "Only admin can request this information.";

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<User> getAll(User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ONLY_ADMIN_ERROR_MESSAGE);
        }
        return employeeRepository.getAll();
    }

    @Override
    public User getById(int id, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ONLY_ADMIN_ERROR_MESSAGE);
        }
        return employeeRepository.getById(id);
    }

//todo only admins
    @Override
    public List<User> search(Optional<String> searchTerm,
                             Optional<String> sort) {
        return employeeRepository.search(searchTerm,sort);
    }

}
