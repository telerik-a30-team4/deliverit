package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotEmpty;

public class ShipmentArrivalDateDto {

    @NotEmpty
    private String arrivalDate;

    public ShipmentArrivalDateDto() {
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
