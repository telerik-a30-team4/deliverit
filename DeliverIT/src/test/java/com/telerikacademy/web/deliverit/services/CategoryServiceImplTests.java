package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.web.deliverit.Helpers.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository mockCategoryRepository;

    @InjectMocks
    CategoryServiceImpl mockCategoryService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockCategoryRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockCategoryService.getAll();

        Mockito.verify(mockCategoryRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository() {
        Category mockCategory = createMockCategory();

        Mockito.when(mockCategoryRepository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);

        Category result = mockCategoryService.getById(mockCategory.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }


}
