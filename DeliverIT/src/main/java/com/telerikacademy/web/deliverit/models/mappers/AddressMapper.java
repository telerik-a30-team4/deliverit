package com.telerikacademy.web.deliverit.models.mappers;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.dtos.AddressDto;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {
    private final CityRepository cityRepository;

    @Autowired
    public AddressMapper(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public Address addressFromDto(AddressDto addressDto) {
        Address address = new Address();
        addressDtoToObject(addressDto, address);

        return address;
    }

    public void addressDtoToObject(AddressDto addressDto, Address address) {
        City city = cityRepository.getById(addressDto.getCityId());
        address.setCity(city);
        address.setStreet(addressDto.getStreet());
    }

}
