package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Warehouse;

import java.util.List;
import java.util.Optional;

public interface WarehouseRepository {
    List<Warehouse> getAll();

    Warehouse getById(int id);

    List<Warehouse> getByCityId(int cityId);

    void create(Warehouse warehouse);

    void update(Warehouse warehouse);

    void delete(int id);

    List<Warehouse> search(Optional<String> searchTerm, Optional<String> sort);
}
