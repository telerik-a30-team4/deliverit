package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParcelServiceImpl implements ParcelService {

    public static final String NOT_THE_NEEDED_CUSTOMER_OR_ADMIN = "Only owner or admin can request, modify or delete.";
    public static final String CUSTOMER_ERROR_MESSAGE = "Only customers can create parcels.";
    public static final String SHIPMENT_ERROR_MESSAGE = "Parcel pick-up cannot be changed, because shipment is already completed.";

    private final ParcelRepository parcelRepository;

    @Autowired
    public ParcelServiceImpl(ParcelRepository parcelRepository) {
        this.parcelRepository = parcelRepository;
    }

    @Override
    public List<Parcel> getAll() {
        return parcelRepository.getAll();
    }

    @Override
    public Parcel getById(int id, User loggedUser) {
        if (parcelRepository.getById(id).getCustomer().getId() != loggedUser.getId() && !loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }
        return parcelRepository.getById(id);
    }

    @Override
    public void create(Parcel parcel, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.USER)) {
            throw new UnauthorizedOperationException(CUSTOMER_ERROR_MESSAGE);
        }

        parcel.setCustomer(loggedUser);
        parcelRepository.create(parcel);
    }

    @Override
    public void update(Parcel parcel, User loggedUser) {
        if (parcel.getCustomer().getId() != loggedUser.getId() && !loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }

        if (parcel.getShipment() == null) {
            parcelRepository.update(parcel);
            return;
        }

        if (parcel.getShipment().getStatus().equals(Status.COMPLETED)) {
            throw new UnauthorizedOperationException(SHIPMENT_ERROR_MESSAGE);
        }

        parcelRepository.update(parcel);

/*        boolean isThereShipment = true;
        try {
            //Shipment shipment = getShipmentByParcelId(parcel.getId(), loggedUser);
            if (parcel.getShipment() == null) {
                throw new EntityNotFoundException()
            }

            if (parcel.getShipment().getStatus().equals(Status.COMPLETED)) {
                throw new UnauthorizedOperationException(SHIPMENT_ERROR_MESSAGE);
            }
        } catch (EntityNotFoundException e) {
            isThereShipment = false;
        }*/

    }

    @Override
    public void delete(int id, User loggedUser) {
        if (parcelRepository.getById(id).getCustomer().getId() != loggedUser.getId() && !loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }
        parcelRepository.delete(id);
    }

    @Override
    public List<Parcel> filter(Optional<Double> minWeight,
                               Optional<Double> maxWeight,
                               Optional<Integer> customerId,
                               Optional<Integer> categoryId,
                               Optional<Integer> warehouseId,
                               Optional<String> destination,
                               Optional<Integer> shipmentStatusId,
                               Optional<String> customerKeyWord,
                               Optional<String> sort) {

        return parcelRepository.filter(minWeight, maxWeight, customerId, categoryId, warehouseId, destination, shipmentStatusId, customerKeyWord, sort);
    }

    @Override
    public List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate) {
        return parcelRepository.sort(weight, arrivalDate);
    }

    @Override
    public Shipment getShipmentByParcelId(int id, User loggedUser) {
        if (parcelRepository.getById(id).getCustomer().getId() != loggedUser.getId() && !loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }
        return parcelRepository.getShipmentByParcelId(id);
    }

    @Override
    public void removeParcelFromShipment(Parcel parcel, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(NOT_THE_NEEDED_CUSTOMER_OR_ADMIN);
        }
        if (!parcelRepository.getShipmentByParcelId(parcel.getId()).getStatus().equals(Status.PREPARING)) {
            throw new UnauthorizedOperationException("Shipment is on the way or already completed.");
        }
        parcelRepository.removeParcelFromShipment(parcel);
    }

}

