package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.contracts.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.web.deliverit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTests {

    @Mock
    EmployeeRepository mockEmployeeRepository;

    @InjectMocks
    EmployeeServiceImpl mockEmployeeService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockEmployeeRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockEmployeeService.getAll(createMockAdmin());

        Mockito.verify(mockEmployeeRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    void getAll_should_throwException_whenUser() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockEmployeeService.getAll(createMockUser()));
    }

    @Test
    void getById_should_callRepository() {
        //Arrange
        User mockAdmin = createMockAdmin();
        User otherMockAdmin = createMockAdmin();
        otherMockAdmin.setId(2);

        Mockito.when(mockEmployeeRepository.getById(mockAdmin.getId()))
                .thenReturn(mockAdmin);

        //Act
        User result = mockEmployeeService.getById(mockAdmin.getId(), otherMockAdmin);

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockAdmin.getId(), result.getId()),
                () -> Assertions.assertEquals(mockAdmin.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockAdmin.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockAdmin.getAddress(), result.getAddress()),
                () -> Assertions.assertEquals(mockAdmin.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockAdmin.getRole(), result.getRole())
        );
    }

    @Test
    void getById_should_throwException_whenLoggedUser() {
        User mockAdmin = createMockAdmin();
        User otherMockUser = createMockUser();
        otherMockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockEmployeeService.getById(mockAdmin.getId(), otherMockUser));
    }

}
