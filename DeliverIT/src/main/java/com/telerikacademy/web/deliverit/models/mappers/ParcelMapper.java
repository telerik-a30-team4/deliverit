package com.telerikacademy.web.deliverit.models.mappers;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.dtos.ParcelDto;
import com.telerikacademy.web.deliverit.models.dtos.ParcelPickUpDto;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParcelMapper {

    private final ParcelRepository parcelRepository;
    private final CustomerRepository customerRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ParcelMapper(ParcelRepository parcelRepository, CustomerRepository customerRepository, CategoryRepository categoryRepository) {
        this.parcelRepository = parcelRepository;
        this.customerRepository = customerRepository;
        this.categoryRepository = categoryRepository;
    }

    public Parcel parcelFromDto(ParcelDto parcelDto) {
        Parcel parcel = new Parcel();
        parcelDtoToObject(parcelDto, parcel);

        return parcel;
    }

    public ParcelDto parcelDtoFromParcel(Parcel parcel){
        ParcelDto parcelDto = new ParcelDto();
        parcelDto.setWeight(parcel.getWeight());
        parcelDto.setCategoryId(parcel.getCategory().getId());
        parcelDto.setPickUpFromWarehouse(parcel.isPickUpFromWarehouse());

        return parcelDto;
    }

    public Parcel parcelFromDto(ParcelDto parcelDto, int id) {
        Parcel parcel = parcelRepository.getById(id);
        parcelDtoToObject(parcelDto, parcel);

        return parcel;
    }

    public Parcel setParcelPickUp(ParcelPickUpDto parcelPickUpDto, int id) {
        Parcel parcel = parcelRepository.getById(id);
        parcel.setPickUpFromWarehouse(parcelPickUpDto.isPickUpFromWarehouse());
        return parcel;        
    }

    private void parcelDtoToObject(ParcelDto parcelDto, Parcel parcel) {
        //User customer = customerRepository.getById(parcelDto.getCustomerId());

        parcel.setWeight(parcelDto.getWeight());
        //parcel.setCustomer(customer);
        parcel.setCategory(categoryRepository.getById(parcelDto.getCategoryId()));
        parcel.setPickUpFromWarehouse(parcelDto.isPickUpFromWarehouse());

    }


}
