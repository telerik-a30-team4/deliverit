package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.AdminFailureException;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.*;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.models.mappers.ShipmentMapper;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.services.contracts.UserService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController {

    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final UserService userService;
    private final WarehouseService warehouseService;
    private final ShipmentMapper shipmentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentMvcController(ShipmentService shipmentService,
                                 ParcelService parcelService,
                                 UserService userService,
                                 WarehouseService warehouseService,
                                 ShipmentMapper shipmentMapper,
                                 AuthenticationHelper authenticationHelper) {
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.userService = userService;
        this.warehouseService = warehouseService;
        this.shipmentMapper = shipmentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return List.of(Status.values());
    }

    @GetMapping
    public String showAllShipments(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        model.addAttribute("shipments", shipmentService.getAll());
        model.addAttribute("shipmentSearchDto", new ShipmentSearchDto());

        return "shipments";
    }

    @GetMapping("/new")
    public String showNewShipmentPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        model.addAttribute("shipment", new ShipmentDto());
        return "shipment-new";
    }

    @PostMapping("/new")
    public String createShipment(@Valid @ModelAttribute("shipment") ShipmentDto shipmentDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        if (errors.hasErrors()) {
            return "shipment-new";
        }

        try {
            Shipment shipment = shipmentMapper.shipmentFromDto(shipmentDto);
            shipmentService.create(shipment, userAdmin);

            return "redirect:/shipments";
        } catch (UnsupportedOperationException e) {
            errors.rejectValue("arrivalWarehouseId", "duplicate_warehouse", e.getMessage());
            return "shipment-new";
        }
    }

    @GetMapping("/{shipmentId}")
    public String showSingleShipment(@PathVariable int shipmentId,
                                     HttpSession session,
                                     Model model) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Shipment shipment = shipmentService.getById(shipmentId);
            List<Parcel> shipmentParcels = shipmentService.getShipmentParcels(shipmentId);
            model.addAttribute("shipment", shipment);
            model.addAttribute("shipmentParcels", shipmentParcels);
            return "shipment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/remove-parcel/{parcelId}")
    public String removeParcelFromShipment(@PathVariable int parcelId,
                                           HttpSession session,
                                           Model model) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Parcel parcelToRemove = parcelService.getById(parcelId, userAdmin);
            Shipment shipment = parcelService.getShipmentByParcelId(parcelId, userAdmin);

            parcelService.removeParcelFromShipment(parcelToRemove, userAdmin);
/*            parcelToRemove.setShipment(null);
            parcelService.update(parcelToRemove, userAdmin);*/

            return String.format("redirect:/shipments/%d", shipment.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @GetMapping("/add-parcel/{parcelId}")
    public String showAddParcelToShipmentPage(@PathVariable int parcelId,
                                              HttpSession session,
                                              Model model) {

        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Parcel parcel = parcelService.getById(parcelId, userAdmin);

            if (parcel.hasShipment()) {
                throw new UnauthorizedOperationException("This parcel is already assigned to a shipment.");
            }

            int warehouseCityId = parcelService.getById(parcelId, userAdmin).getCustomer().getAddress().getCity().getId();
            int warehouseId = warehouseService.getByCityId(warehouseCityId).get(0).getId();

            List<Shipment> filteredShipments = shipmentService.filter(Optional.of(warehouseId), Optional.of("arr"), Optional.empty(), Optional.empty(), Optional.of(0), Optional.empty());
            model.addAttribute("shipments", filteredShipments);
            model.addAttribute("parcel", parcel);

            return "shipment-add-Parcel";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @GetMapping("/{shipmentId}/add-parcel/{parcelId}")
    public String addParcelToShipment(@PathVariable int shipmentId,
                                      @PathVariable int parcelId,
                                      HttpSession session,
                                      Model model) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Parcel parcelToAdd = parcelService.getById(parcelId, userAdmin);
            Shipment shipment = shipmentService.getById(shipmentId);

            if (parcelToAdd.hasShipment()) {
                throw new UnauthorizedOperationException("This parcel is already assigned to a shipment.");
            }

            shipmentService.addParcel(shipment, parcelToAdd, userAdmin);

            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @GetMapping("/{shipmentId}/update")
    public String showUpdateShipmentPage(@PathVariable int shipmentId,
                                         HttpSession session,
                                         Model model) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Shipment shipment = shipmentService.getById(shipmentId);

            if (shipment.getStatus().equals(Status.COMPLETED)) {
                throw new UnauthorizedOperationException("You can't modify an already completed shipment.");
            }

            List<Parcel> shipmentParcels = shipmentService.getShipmentParcels(shipmentId);

            ShipmentDto shipmentDto = new ShipmentDto();
            shipmentDto.setDepartureWarehouseId(shipment.getDepartureWarehouse().getId());
            shipmentDto.setArrivalWarehouseId(shipment.getArrivalWarehouse().getId());
            model.addAttribute("shipmentDto", shipmentDto);
            model.addAttribute("shipment", shipment);

            if (shipmentParcels.isEmpty()) {
                return "shipment-update-warehouses";
            } else if (shipment.getDepartureDate() == null) {
                return "shipment-update-DepartureDate";
            } else {
                return "shipment-update-ArrivalDate";
            }

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @PostMapping("/{shipmentId}/update")
    public String updateShipment(@Valid @ModelAttribute("shipmentDto") ShipmentDto shipmentDto,
                                 @PathVariable int shipmentId,
                                 BindingResult errors,
                                 HttpSession session,
                                 Model model) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        if (errors.hasErrors()) {
            return "shipment-update-warehouses";
        }

        try {
            Shipment shipment = shipmentMapper.shipmentFromDto(shipmentDto, shipmentId);
            shipmentService.update(shipment, userAdmin);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{shipmentId}/update-departureDate")
    public String updateShipmentDepartureDate(@Valid @ModelAttribute("shipmentDto") ShipmentDepartureDateDto shipmentDepartureDateDto,
                                              @PathVariable int shipmentId,
                                              BindingResult errors,
                                              HttpSession session,
                                              Model model) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        if (errors.hasErrors()) {
            return "shipment-update-DepartureDate";
        }

        try {
            Shipment shipment = shipmentMapper.setDepartureDate(shipmentDepartureDateDto, shipmentId);
            shipmentService.update(shipment, userAdmin);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @PostMapping("/{shipmentId}/update-arrivalDate")
    public String updateShipmentArrivalDate(@Valid @ModelAttribute("shipmentDto") ShipmentArrivalDateDto shipmentArrivalDateDto,
                                            @PathVariable int shipmentId,
                                            BindingResult errors,
                                            HttpSession session,
                                            Model model) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        if (errors.hasErrors()) {
            return "shipment-update-ArrivalDate";
        }

        try {
            Shipment shipment = shipmentMapper.setArrivalDate(shipmentArrivalDateDto, shipmentId);
            shipmentService.update(shipment, userAdmin);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @GetMapping("/{shipmentId}/delete")
    public String deleteParcel(@PathVariable int shipmentId,
                               HttpSession session,
                               Model model) {
        User userAdmin;
        try {
            userAdmin = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Shipment shipment = shipmentService.getById(shipmentId);

            if (shipment.isModifiable()) {
                shipmentService.delete(shipmentId, userAdmin);
                return "redirect:/shipments";
            } else {
                throw new UnauthorizedOperationException("You can't delete a departed shipment.");
            }

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @PostMapping("/search")
    public String searchForShipment(@ModelAttribute ShipmentSearchDto shipmentSearchDto,
                                    HttpSession session,
                                    Model model) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        Optional<Integer> warehouseId = shipmentSearchDto.getWarehouseId() != -1 ? Optional.ofNullable(shipmentSearchDto.getWarehouseId()) : Optional.empty();
        Optional<String> destination = shipmentSearchDto.getDestination().equals("-1") ? Optional.empty() : Optional.ofNullable(shipmentSearchDto.getDestination());
        Optional<Integer> statusId = shipmentSearchDto.getStatusId() != -1 ? Optional.ofNullable(shipmentSearchDto.getStatusId()) : Optional.empty();

        List<Shipment> search = shipmentService.filter(
                warehouseId,
                destination,
                Optional.empty(),
                Optional.ofNullable(shipmentSearchDto.getEmployeeKeyWord()),
                statusId,
                Optional.ofNullable(shipmentSearchDto.getSort()));

        model.addAttribute("shipments", search);

        return "shipments";
    }
}
