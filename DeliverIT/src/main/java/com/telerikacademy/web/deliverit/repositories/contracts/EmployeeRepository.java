package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {
    List<User> getAll();

    User getById(int id);

    List<User> search(Optional<String> searchTerm, Optional<String> sort);
}
