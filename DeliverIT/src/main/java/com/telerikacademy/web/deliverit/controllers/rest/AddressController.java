package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.dtos.AddressDto;
import com.telerikacademy.web.deliverit.services.contracts.AddressService;
import com.telerikacademy.web.deliverit.models.mappers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/addresses")
public class AddressController {

    private final AddressService addressService;
    private final AddressMapper addressMapper;

    @Autowired
    public AddressController(AddressService addressService, AddressMapper addressMapper) {
        this.addressService = addressService;
        this.addressMapper = addressMapper;
    }

    @GetMapping
    public List<Address> getAll() {
        return addressService.getAll();
    }

    @PostMapping
    public Address create(@Valid @RequestBody AddressDto addressDto) {
        try {
            Address address = addressMapper.addressFromDto(addressDto);
            addressService.create(address);
            return address;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/street")
    public Address getByStreet(String street) {
        street = "bul Tsarigradsko shose 801";
        try {
            return addressService.getByStreet(street);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
