package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class AddressDto {

    @NotNull(message = "Address can't be empty")
    @Size(min = 3, max = 45, message = "Address should be between 3 and 45 symbols")
    private String street;

    @NotNull(message = "Address city can't be empty")
    @Positive
    private int cityId;

    public AddressDto() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
