package com.telerikacademy.web.deliverit.models.dtos;

public class ParcelSearchDto {

    private Double minWeight;
    private Double maxWeight;
    private Integer categoryId;
    private Integer warehouseId;
    private String destination;
    private Integer shipmentStatusId;
    private String customerKeyWord;
    private String sort;

    public ParcelSearchDto() {
    }

    public Double getMinWeight() {
        return minWeight;
    }

    public void setMinWeight(Double minWeight) {
        this.minWeight = minWeight;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getShipmentStatusId() {
        return shipmentStatusId;
    }

    public void setShipmentStatusId(Integer shipmentStatusId) {
        this.shipmentStatusId = shipmentStatusId;
    }

    public String getCustomerKeyWord() {
        return customerKeyWord;
    }

    public void setCustomerKeyWord(String customerKeyWord) {
        this.customerKeyWord = customerKeyWord;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
