package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {
    List<Shipment> getAll();

    Shipment getById(int id);

    void create(Shipment shipment);

    void update(Shipment shipment);

    void delete(int id);

    List<Shipment> filter(Optional<Integer> warehouseId,
                          Optional<String> destination,
                          Optional<Integer> customerId,
                          Optional<String> employeeKeyWord,
                          Optional<Integer> statusId,
                          Optional<String> sort);

    List<Parcel> getShipmentParcels(int id);

    void addParcel(Shipment shipment, Parcel parcel);

    List<Shipment> search(String searchTerm);
}
