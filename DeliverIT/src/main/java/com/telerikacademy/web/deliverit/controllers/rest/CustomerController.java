package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.UserDto;
import com.telerikacademy.web.deliverit.models.dtos.DisplayInfoParcelDto;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.models.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerController(CustomerService customerService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.customerService = customerService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return customerService.getAll();
    }

    @GetMapping("/count")
    public Integer getCustomerCount() {
        return customerService.getAll().size();
    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            return customerService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> email,
                             @RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> keyWord,
                             @RequestParam(required = false) Optional<String> sort) {
        try {
            authenticationHelper.tryGetUser(headers);
            return customerService.search(email, firstName, lastName, keyWord, sort);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{customerId}/parcels")
    public List<DisplayInfoParcelDto> showParcelsByCustomerId(@RequestHeader HttpHeaders headers,
                                                              @PathVariable int customerId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.showParcelsByCustomerId(customerId, user)
                    .stream()
                    .map(parcel -> new DisplayInfoParcelDto(parcel.getId(), parcel.getWeight(), parcel.getCategory().getId(), parcel.getCustomer().getId(), parcel.isPickUpFromWarehouse()))
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.userFromDto(userDto);
            customerService.create(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody UserDto userDto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            User userToUpdate = userMapper.userFromDto(userDto, id);
            customerService.update(userToUpdate, loggedUser);
            return userToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            customerService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
