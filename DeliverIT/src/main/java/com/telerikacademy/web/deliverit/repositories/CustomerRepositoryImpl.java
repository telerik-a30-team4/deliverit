package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where u.role=1", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null || !user.getRole().equals(Role.USER)) {
                throw new EntityNotFoundException("Customer", "id", String.valueOf(id));
            }
            return user;
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User user = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> search(Optional<String> email,
                             Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> keyWord,
                             Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {

            String queryString = "from User where role=1";

            //Search by email
            if (email.isPresent()) queryString += " AND email like ?1 ";

            //Search by first name
            if (firstName.isPresent()) queryString += " AND firstName = :firstName";

            //Search by last name
            if (lastName.isPresent()) queryString += " AND lastName = :lastName";

            //Search fields email, first name, last name for a key word
//            if (keyWord.isPresent()) queryString += " AND (email like ?1 OR firstName like ?1 OR lastName like ?1)";
            if (keyWord.isPresent()) queryString += " and concat(firstName, ' ', lastName, ' ', email) like ?1";

            if (sort.isPresent()) queryString += " order by " + sort.get();

            Query<User> query = session.createQuery(queryString, User.class);

            if (email.isPresent()) query.setParameter(1, "%" + email.get() + "%");
            if (firstName.isPresent()) query.setParameter("firstName", firstName.get());
            if (lastName.isPresent()) query.setParameter("lastName", lastName.get());
            if (keyWord.isPresent()) query.setParameter(1, "%" + keyWord.get() + "%");
//            if (sort.isPresent()) query.setParameter(3, sort.get());

            return query.list();

        }
    }

    //ToDo - make status as optional parameter - ? dublira se s parcels/filter
    @Override
    public List<Parcel> showParcelsByCustomerId(int customerId) {
        try (Session session = sessionFactory.openSession()) {
            getById(customerId);

            String queryString = "select p from Parcel p where p.customer.id = :id and p.shipment.status = :status";

            Query<Parcel> query = session.createQuery(queryString, Parcel.class);

            Status status = Status.ON_THE_WAY;
            query.setParameter("id", customerId);
            query.setParameter("status", status);

            return query.list();
        }
    }

}
