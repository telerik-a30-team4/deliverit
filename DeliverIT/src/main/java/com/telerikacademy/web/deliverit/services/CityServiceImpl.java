package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> getAll() {
        return cityRepository.getAll();
    }
}
