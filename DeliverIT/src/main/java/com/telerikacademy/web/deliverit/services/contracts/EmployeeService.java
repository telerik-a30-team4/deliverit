package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    List<User> getAll(User loggedUser);

    User getById(int id, User loggedUser);

    List<User> search(Optional<String> searchTerm, Optional<String> sort);
}
