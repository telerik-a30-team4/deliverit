package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {
    List<Parcel> getAll();

    Parcel getById(int id);

    void create(Parcel parcel);

    void update(Parcel parcel);

    void delete(int id);

    List<Parcel> filter(Optional<Double> minWeight,
                        Optional<Double> maxWeight,
                        Optional<Integer> customerId,
                        Optional<Integer> categoryId,
                        Optional<Integer> warehouseId,
                        Optional<String> destination,
                        Optional<Integer> shipmentStatusId,
                        Optional<String> customerKeyWord,
                        Optional<String> sort);

    List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate);

    Shipment getShipmentByParcelId(int id);

    void removeParcelFromShipment(Parcel parcel);
}

