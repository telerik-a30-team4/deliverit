package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();

    Category getById(int id);
}
