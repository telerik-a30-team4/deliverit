package com.telerikacademy.web.deliverit.models;

import com.telerikacademy.web.deliverit.models.enums.Status;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "parcels")
public class Parcel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "parcel_id")
    private int id;

    @Column(name = "weight")
    private double weight;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private User customer;

    @Column(name = "pick_up_from_warehouse")
    private boolean pickUpFromWarehouse = true;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment shipment;

    public Parcel() {
    }

    public Parcel(double weight, Category category, boolean pickUpFromWarehouse) {
        this.weight = weight;
        this.category = category;
        this.pickUpFromWarehouse = pickUpFromWarehouse;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public boolean isPickUpFromWarehouse() {
        return pickUpFromWarehouse;
    }

    public void setPickUpFromWarehouse(boolean pickUpFromWarehouse) {
        this.pickUpFromWarehouse = pickUpFromWarehouse;
    }

    public boolean hasShipment(){
        return getShipment() != null;
    }

    public boolean hasShipmentInPreparing() {
        return hasShipment() && getShipment().getStatus().equals(Status.PREPARING);
    }

    public boolean isPickUpFromWarehouseModifiable() {
        if (hasShipment() && getShipment().getStatus().equals(Status.COMPLETED)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcel parcel = (Parcel) o;
        return id == parcel.id;
    }
}