create table categories
(
    category_id int auto_increment
        primary key,
    category_name varchar(30) not null,
    constraint categories_category_name_uindex
        unique (category_name)
);

create table countries
(
    country_id int auto_increment
        primary key,
    country_name varchar(30) not null,
    constraint countries_country_name_uindex
        unique (country_name)
);

create table cities
(
    city_id int auto_increment
        primary key,
    city_name varchar(30) not null,
    country_id int not null,
    constraint cities_countries_country_id_fk
        foreign key (country_id) references countries (country_id)
);

create table addresses
(
    address_id int auto_increment
        primary key,
    street varchar(40) not null,
    city_id int not null,
    constraint addresses_cities_city_id_fk
        foreign key (city_id) references cities (city_id)
);

create table users
(
    user_id int auto_increment
        primary key,
    first_name varchar(15) not null,
    last_name varchar(15) not null,
    e_mail varchar(40) not null,
    role_id int not null,
    address_id int not null,
    constraint users_e_mail_uindex
        unique (e_mail),
    constraint users_addresses_address_id_fk
        foreign key (address_id) references addresses (address_id)
);

create table parcels
(
    parcel_id int auto_increment
        primary key,
    weight double not null,
    category_id int not null,
    customer_id int not null,
    pick_up_from_warehouse tinyint(1) default 1 not null,
    constraint parcels_categories_fk
        foreign key (category_id) references categories (category_id),
    constraint parcels_user_details_user_id_fk
        foreign key (customer_id) references users (user_id)
);

create index parcels_customers_customer_id_fk
    on parcels (customer_id);

create index users_roles_role_id_fk
    on users (role_id);

create table warehouses
(
    warehouse_id int auto_increment
        primary key,
    address_id int not null,
    constraint warehouses_addresses_fk
        foreign key (address_id) references addresses (address_id)
);

create table shipments
(
    shipment_id int auto_increment
        primary key,
    departure_date date null,
    arrival_date date null,
    status_id int not null,
    employee_id int not null,
    departure_warehouse_id int not null,
    arrival_warehouse_id int not null,
    constraint shipments_arrival_warehouses_fk
        foreign key (arrival_warehouse_id) references warehouses (warehouse_id),
    constraint shipments_employees_fk
        foreign key (employee_id) references users (user_id),
    constraint shipments_warehouses_fk
        foreign key (departure_warehouse_id) references warehouses (warehouse_id)
);

create table order_details
(
    order_detail_id int auto_increment
        primary key,
    shipment_id int not null,
    parcel_id int not null,
    constraint order_details_parcels_fk
        foreign key (parcel_id) references parcels (parcel_id),
    constraint order_details_shipments_fk
        foreign key (shipment_id) references shipments (shipment_id)
);

create index shipments_statuses_fk
    on shipments (status_id);

