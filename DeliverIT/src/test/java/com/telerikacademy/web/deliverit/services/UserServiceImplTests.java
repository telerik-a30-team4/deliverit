package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.web.deliverit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl mockUserService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockUserService.getAll();

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getByEmail_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        User result = mockUserService.getByEmail(mockUser.getEmail());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getAddress(), result.getAddress()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole())
        );
    }

}
