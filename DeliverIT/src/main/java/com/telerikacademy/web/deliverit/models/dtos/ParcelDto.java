package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ParcelDto {

    @NotNull
    @Positive(message = "Weight should be positive.")
    private double weight;

    @NotNull
    @Positive(message = "Category ID should be positive.")
    private int categoryId;

    private boolean pickUpFromWarehouse = true;

    public ParcelDto() {
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isPickUpFromWarehouse() {
        return pickUpFromWarehouse;
    }

    public void setPickUpFromWarehouse(boolean pickUpFromWarehouse) {
        this.pickUpFromWarehouse = pickUpFromWarehouse;
    }
}
