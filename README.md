<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# **DeliverIT**

### Freight Forwarding Management System

![picture](images/DeliverIt-Logo.JPG) 

### Project Description
This is a web application that serves the needs of a freight forwarding company.<br/>

DeliverIT's customers can place orders on international shopping sites (like Amazon.de or eBay.com) and have their parcels delivered either to the company's warehouses or their own address.<br/>

DeliverIT has two types of users - customers and employees. The customers can see how many parcels they have on the way. The employees have a bit more capabilities. They can add new parcels to the system, modify existing ones, check how many parcels has a given warehouse received and more.

### Public Part

The public part is accessible without authentication i.e. for anonymous users.<br/>
Anonymous users are able to see how many customers DeliverIT has and what and where are the available warehouse locations.<br/>
Also, anonymous users have the ability to register.<br/>

### Private part
Accessible only if the user is authenticated.<br/>
Customers are able to see their parcels filtered by different attributes.<br/>
Customers have the ability to see details about the shipment that holds their parcel.<br/>
Customers have the ability to pick between “pick up” or “delivery” for each parcel, but only if shipment status is not “completed”.<br/>

### Administrative part
Accessible to employees only.<br/>
Employees are able to create/modify warehouses.<br/>
Employees are able to create/modify parcels.<br/>
Employees are able to create/modify shipments.<br/>
Employees are able to add/remove parcels from a shipment.<br/>
Employees are able to see how many shipments are on the way.<br/>
Employees are able to see which the next shipment is to arrive for a given warehouse.<br/>

### REST API
To provide other developers with our service, we have developed a REST API.<br/>
[Link to the Swagger documentation](http://localhost:8080/swagger-ui.html) <br/>

### GitLab
[Link to our project](https://gitlab.com/telerik-first-project-group/deliverit) <br/>

### Database relations
![picture](images/DeliverIt-Database-relations.JPG) 