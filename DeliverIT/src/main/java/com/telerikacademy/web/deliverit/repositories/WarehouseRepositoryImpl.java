package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public WarehouseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Warehouse> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Warehouse> query = session.createQuery("from Warehouse", Warehouse.class);

            return query.list();
        }
    }

    @Override
    public Warehouse getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Warehouse warehouse = session.get(Warehouse.class, id);
            if (warehouse == null) {
                throw new EntityNotFoundException("Warehouse", "id", String.valueOf(id));
            }
            return warehouse;
        }
    }

    @Override
    public List<Warehouse> getByCityId(int cityId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Warehouse> query = session.createQuery("from Warehouse where address.city.id = :cityId", Warehouse.class);
            query.setParameter("cityId", cityId);

            return query.list();
        }
    }

    @Override
    public void create(Warehouse warehouse) {
        try (Session session = sessionFactory.openSession()) {
            session.save(warehouse);
        }
    }

    @Override
    public void update(Warehouse warehouse) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(warehouse);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Warehouse warehouseToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(warehouseToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Warehouse> search(Optional<String> searchTerm, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Warehouse where 1=1";

            if (searchTerm.isPresent()) queryString += " and concat(address, ' ', address.city.name, ' ', address.city.country.name) like ?1 ";
            if (sort.isPresent()) queryString += " order by " + sort.get();

            Query<Warehouse> query = session.createQuery(queryString, Warehouse.class);

            if (searchTerm.isPresent()) query.setParameter(1, "%" + searchTerm.get() + "%");

            return query.list();
        }
    }
}
