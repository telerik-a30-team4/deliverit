package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class DisplayInfoParcelDto {

    private int parcelId;

    private double weight;

    private int categoryId;

    private int customerId;

    private boolean pickUpFromWarehouse;

    public DisplayInfoParcelDto() {
    }

    public DisplayInfoParcelDto(int parcelId, double weight, int categoryId, int customerId, boolean pickUpFromWarehouse) {
        this.parcelId = parcelId;
        this.weight = weight;
        this.categoryId = categoryId;
        this.customerId = customerId;
        this.pickUpFromWarehouse = pickUpFromWarehouse;
    }

    public int getParcelId() {
        return parcelId;
    }

    public void setParcelId(int parcelId) {
        this.parcelId = parcelId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public boolean isPickUpFromWarehouse() {
        return pickUpFromWarehouse;
    }

    public void setPickUpFromWarehouse(boolean pickUpFromWarehouse) {
        this.pickUpFromWarehouse = pickUpFromWarehouse;
    }
}
