package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Address;

import java.util.List;

public interface AddressRepository {
    List<Address> getAll();

    Address getById(int id);

    void create(Address address);

    Address getByStreet(String street);
}
