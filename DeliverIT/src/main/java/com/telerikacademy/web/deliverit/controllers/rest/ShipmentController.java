package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.ShipmentArrivalDateDto;
import com.telerikacademy.web.deliverit.models.dtos.ShipmentDepartureDateDto;
import com.telerikacademy.web.deliverit.models.dtos.ShipmentDto;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import com.telerikacademy.web.deliverit.models.mappers.ShipmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/shipments")
public class ShipmentController {

    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final ShipmentMapper shipmentMapper;
    private final AuthenticationHelper authenticationHelper;
    private final WarehouseService warehouseService;

    @Autowired
    public ShipmentController(ShipmentService shipmentService, ParcelService parcelService, ShipmentMapper shipmentMapper, AuthenticationHelper authenticationHelper, WarehouseService warehouseService) {
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.shipmentMapper = shipmentMapper;
        this.authenticationHelper = authenticationHelper;
        this.warehouseService = warehouseService;
    }

    @GetMapping
    public List<Shipment> getAllShipments(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);

        return shipmentService.getAll();
    }

    @GetMapping("/{id}")
    public Shipment findShipmentById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);

        try {
            return shipmentService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/parcels")
    public List<Parcel> getShipmentParcels(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);

        try {
            return shipmentService.getShipmentParcels(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Shipment> filter(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) Optional<Integer> warehouseId,
                                 @RequestParam(required = false) Optional<String> destination,
                                 @RequestParam(required = false) Optional<Integer> customerId,
                                 @RequestParam(required = false) Optional<String> employeeKeyWord,
                                 @RequestParam(required = false) Optional<Integer> statusId) {
        authenticationHelper.tryGetUser(headers);

        try {
            return shipmentService.filter(warehouseId, destination, customerId, employeeKeyWord, statusId, Optional.empty());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Shipment create(@RequestHeader HttpHeaders headers,
                           @RequestBody @Valid ShipmentDto shipmentDto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            Shipment shipment = shipmentMapper.shipmentFromDto(shipmentDto);
            shipmentService.create(shipment, loggedUser);

            return shipment;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Shipment update(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @RequestBody @Valid ShipmentDto shipmentDto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            Shipment shipment = shipmentMapper.shipmentFromDto(shipmentDto, id);
            shipmentService.update(shipment, loggedUser);

            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/arrival-date")
    public Shipment updateArrivalDate(@RequestHeader HttpHeaders headers,
                                      @PathVariable int id,
                                      @RequestBody @Valid ShipmentArrivalDateDto shipmentArrivalDateDto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            Shipment shipment = shipmentMapper.setArrivalDate(shipmentArrivalDateDto, id);
            shipmentService.update(shipment, loggedUser);

            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/departure-date")
    public Shipment updateDepartureDate(@RequestHeader HttpHeaders headers,
                                        @PathVariable int id,
                                        @Valid @RequestBody ShipmentDepartureDateDto shipmentDepartureDateDto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            Shipment shipment = shipmentMapper.setDepartureDate(shipmentDepartureDateDto, id);
            shipmentService.update(shipment, loggedUser);

            return shipment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{shipmentId}/parcel/{parcelId}")
    public void addParcelToShipment(@RequestHeader HttpHeaders headers,
                                    @PathVariable int shipmentId,
                                    @PathVariable int parcelId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            Shipment shipment = shipmentService.getById(shipmentId);
            Parcel parcel = parcelService.getById(parcelId, loggedUser);

            shipmentService.addParcel(shipment, parcel, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteShipment(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            shipmentService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
