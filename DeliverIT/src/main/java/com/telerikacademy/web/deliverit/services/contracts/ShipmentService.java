package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface ShipmentService {
    List<Shipment> getAll();

    Shipment getById(int id);

    void create(Shipment shipment, User loggedUser);

    void update(Shipment shipment, User loggedUser);

    void delete(int id, User loggedUser);

    List<Shipment> filter(Optional<Integer> warehouseId,
                          Optional<String> destination,
                          Optional<Integer> customerId,
                          Optional<String> employeeKeyWord,
                          Optional<Integer> statusId,
                          Optional<String> sort);

    List<Parcel> getShipmentParcels(int id);

    void addParcel(Shipment shipment, Parcel parcel, User loggedUser);

    public List<Shipment> search(String searchTerm);
}
