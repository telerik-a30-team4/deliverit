package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.AdminFailureException;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.ParcelDto;
import com.telerikacademy.web.deliverit.models.dtos.ParcelPickUpDto;
import com.telerikacademy.web.deliverit.models.dtos.ParcelSearchDto;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.models.mappers.ParcelMapper;
import com.telerikacademy.web.deliverit.services.contracts.CategoryService;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.UserService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/parcels")
public class ParcelMvcController {

    private final ParcelService parcelService;
    private final CategoryService categoryService;
    private final UserService userService;
    private final ParcelMapper parcelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final WarehouseService warehouseService;

    @Autowired
    public ParcelMvcController(ParcelService parcelService,
                               CategoryService categoryService,
                               UserService userService,
                               ParcelMapper parcelMapper,
                               AuthenticationHelper authenticationHelper,
                               WarehouseService warehouseService) {
        this.parcelService = parcelService;
        this.categoryService = categoryService;
        this.userService = userService;
        this.parcelMapper = parcelMapper;
        this.authenticationHelper = authenticationHelper;
        this.warehouseService = warehouseService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @GetMapping
    public String showAllParcels(HttpSession session,
                                 Model model) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        model.addAttribute("parcels", parcelService.getAll());
        model.addAttribute("parcelSearchDto", new ParcelSearchDto());
        return "parcels";
    }

    @GetMapping("/personal")
    public String showUserParcels(HttpSession session,
                                  Model model) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        model.addAttribute("parcels", parcelService.filter(
                Optional.empty(),
                Optional.empty(),
                Optional.of((Integer) session.getAttribute("userId")),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()));

        return "parcels-user";
    }

    @GetMapping("/new")
    public String showNewParcelPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        model.addAttribute("parcel", new ParcelDto());
        return "parcel-new";
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "parcel-new";
        }

        Parcel newParcel = parcelMapper.parcelFromDto(parcelDto);
        parcelService.create(newParcel, user);

        if (user.getRole().equals(Role.USER)) {
            return "redirect:/parcels/personal";
        }
        return "redirect:/parcels";
    }

    @GetMapping("/{parcelId}/update")
    public String showUpdateParcelPage(@PathVariable int parcelId,
                                       HttpSession session,
                                       Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        try {
            Parcel parcel = parcelService.getById(parcelId, user);
            ParcelDto parcelDto = parcelMapper.parcelDtoFromParcel(parcel);
            model.addAttribute("parcel", parcelDto);

            if (parcel.getShipment() == null) {
                return "parcel-update";
            } else if (!parcel.getShipment().getStatus().equals(Status.COMPLETED)) {
                model.addAttribute("parcelPickUpDto", new ParcelPickUpDto());
                model.addAttribute("parcelCategory", parcel.getCategory().getName());
                return "parcel-update-pickup";
            } else {
                throw new UnauthorizedOperationException("You can't modify a parcel in a completed shipment.");
            }

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{parcelId}/update")
    public String updateParcel(@PathVariable int parcelId,
                               @Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "parcel-update";
        }

        try {
            Parcel parcelToUpdate = parcelMapper.parcelFromDto(parcelDto, parcelId);
            parcelService.update(parcelToUpdate, user);

            if (user.getRole().equals(Role.USER)) {
                return "redirect:/parcels/personal";
            }
            return "redirect:/parcels";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @PostMapping("/{parcelId}/update-pickup")
    public String updateParcelPickUp(@PathVariable int parcelId,
                                     @Valid @ModelAttribute("parcelPickUpDto") ParcelPickUpDto parcelPickUpDto,
                                     BindingResult errors,
                                     HttpSession session,
                                     Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "parcel-update-pickup";
        }

        try {
            Parcel parcelToUpdate = parcelMapper.setParcelPickUp(parcelPickUpDto, parcelId);
            parcelService.update(parcelToUpdate, user);

            if (user.getRole().equals(Role.USER)) {
                return "redirect:/parcels/personal";
            }
            return "redirect:/parcels";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteParcel(@PathVariable int id,
                               HttpSession session,
                               Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        }

        try {
            Parcel parcelToDelete = parcelService.getById(id, user);

            if (parcelToDelete.getShipment() == null) {
                parcelService.delete(id, user);
                return "redirect:/parcels";
            } else {
                throw new UnauthorizedOperationException("You can't delete a parcel assigned to a shipment.");
            }

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
    }

    @PostMapping("/search")
    public String searchForParcel(@ModelAttribute ParcelSearchDto parcelSearchDto,
                                  HttpSession session,
                                  Model model) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        Optional<Double> maxWeight = parcelSearchDto.getMaxWeight() != null ? Optional.ofNullable(parcelSearchDto.getMaxWeight()) : Optional.empty();
        Optional<Integer> categoryId = parcelSearchDto.getCategoryId() != -1 ? Optional.ofNullable(parcelSearchDto.getCategoryId()) : Optional.empty();
        Optional<Integer> warehouseId = parcelSearchDto.getWarehouseId() != -1 ? Optional.ofNullable(parcelSearchDto.getWarehouseId()) : Optional.empty();
        Optional<String> destination = parcelSearchDto.getDestination().equals("-1") ? Optional.empty() : Optional.ofNullable(parcelSearchDto.getDestination());
        Optional<Integer> shipmentStatusId = parcelSearchDto.getShipmentStatusId() != -1 ? Optional.ofNullable(parcelSearchDto.getShipmentStatusId()) : Optional.empty();

        List<Parcel> search = parcelService.filter(
                Optional.ofNullable(parcelSearchDto.getMinWeight()),
                maxWeight,
                Optional.empty(),
                categoryId,
                warehouseId,
                destination,
                shipmentStatusId,
                Optional.ofNullable(parcelSearchDto.getCustomerKeyWord()),
                Optional.ofNullable(parcelSearchDto.getSort()));

        model.addAttribute("parcels", search);

        return "parcels";
    }

}