package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class WarehouseDto {

    @NotNull(message = "Warehouse address cant be empty")
    @Positive
    private int addressId;

    public WarehouseDto() {
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }
}
