package com.telerikacademy.web.deliverit.models.mappers;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.WarehouseDto;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WarehouseMapper {

    private final WarehouseRepository warehouseRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public WarehouseMapper(WarehouseRepository warehouseRepository, AddressRepository addressRepository) {
        this.warehouseRepository = warehouseRepository;
        this.addressRepository = addressRepository;
    }


    public Warehouse warehouseFromDto(WarehouseDto warehouseDto) {
        Warehouse warehouse = new Warehouse();
        warehouseDtoToObject(warehouseDto, warehouse);

        return warehouse;
    }

    public Warehouse warehouseFromDto(WarehouseDto warehouseDto, int id) {
        Warehouse warehouse = warehouseRepository.getById(id);
        warehouseDtoToObject(warehouseDto, warehouse);

        return warehouse;
    }

    private void warehouseDtoToObject(WarehouseDto warehouseDto, Warehouse warehouse) {
        Address address = addressRepository.getById(warehouseDto.getAddressId());
        warehouse.setAddress(address);
    }

}
