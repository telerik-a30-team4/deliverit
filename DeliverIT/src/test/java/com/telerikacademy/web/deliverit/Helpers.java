package com.telerikacademy.web.deliverit;

import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.models.enums.Status;

public class Helpers {

    public static Shipment createMockShipment() {

        var mockShipment = new Shipment();
        mockShipment.setId(1);
        mockShipment.setEmployee(createMockAdmin());
        mockShipment.setArrivalWarehouse(createMockWarehouse());
        mockShipment.setDepartureWarehouse(createMockDepartureWarehouse());
        mockShipment.setStatus(Status.PREPARING);

        return mockShipment;
    }

    public static Parcel createMockParcel() {
        var mockParcel = new Parcel();
        mockParcel.setId(1);
        mockParcel.setCustomer(createMockUser());
        mockParcel.setCategory(createMockCategory());
        mockParcel.setWeight(1.1);
        mockParcel.setPickUpFromWarehouse(false);

        return mockParcel;
    }

    public static Warehouse createMockWarehouse() {
        var mockWarehouse = new Warehouse();
        mockWarehouse.setId(1);
        mockWarehouse.setAddress(createMockAddress());

        return mockWarehouse;
    }

    public static Warehouse createMockDepartureWarehouse() {
        var mockWarehouse = new Warehouse();
        mockWarehouse.setId(2);
        mockWarehouse.setAddress(createMockAddress());

        return mockWarehouse;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");

        return mockCategory;
    }

    public static Address createMockAddress() {
        var mockCountry = new Country();
        mockCountry.setId(1);
        mockCountry.setName("MockCountry");

        var mockCity = new City();
        mockCity.setId(1);
        mockCity.setName("MockCity");
        mockCity.setCountry(mockCountry);

        var mockAddress = new Address();
        mockAddress.setId(1);
        mockAddress.setStreet("Mock Street");
        mockAddress.setCity(mockCity);

        return mockAddress;
    }

    public static User createMockUser() {
        return createMockUser("USER");
    }

    public static User createMockAdmin() {
        return createMockUser("ADMIN");
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setRole(Role.valueOf(role));
        mockUser.setAddress(createMockAddress());

        return mockUser;
    }

}
