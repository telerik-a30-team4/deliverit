package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.AdminFailureException;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.SearchTermDto;
import com.telerikacademy.web.deliverit.models.dtos.WarehouseDto;
import com.telerikacademy.web.deliverit.models.mappers.WarehouseMapper;
import com.telerikacademy.web.deliverit.services.contracts.UserService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {

    private final WarehouseService warehouseService;
    private final WarehouseMapper warehouseMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseMvcController(WarehouseService warehouseService,
                                  WarehouseMapper warehouseMapper,
                                  AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseMapper = warehouseMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllWarehouses(Model model) {
        model.addAttribute("warehouses", warehouseService.getAll());
        model.addAttribute("searchTermDto", new SearchTermDto());
        return "warehouses";
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable int id, Model model) {
        try {
            Warehouse warehouse = warehouseService.getById(id);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewWarehousePage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
        model.addAttribute("warehouse", new WarehouseDto());
        return "warehouse-new";
    }

    @PostMapping("/new")
    public String createWarehouse(@Valid @ModelAttribute("warehouse") WarehouseDto warehouseDto,
                                  BindingResult errors,
                                  HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            return "denied";
        }

        if (errors.hasErrors()) {
            return "warehouse-new";
        }

        try {
            Warehouse warehouse = warehouseMapper.warehouseFromDto(warehouseDto);
            warehouseService.create(warehouse, user);

            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("addressId", "duplicate_address", e.getMessage());
            return "warehouse-new";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("addressId", "entity-not-found", e.getMessage());
            return "warehouse-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateWarehousePage(@PathVariable int id,
                                          Model model,
                                          HttpSession session) {
        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }

        try {
            Warehouse warehouse = warehouseService.getById(id);
            WarehouseDto warehouseDto = new WarehouseDto();
            warehouseDto.setAddressId(warehouse.getAddress().getId());
            model.addAttribute("addressId", warehouseDto.getAddressId());
            model.addAttribute("warehouse", warehouseDto);
            return "warehouse-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateWarehouse(@PathVariable int id,
                                  @Valid @ModelAttribute("warehouse") WarehouseDto warehouseDto,
                                  BindingResult errors,
                                  HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            return "denied";
        }

        if (errors.hasErrors()) {
            return "warehouse-update";
        }

        try {
            Warehouse warehouse = warehouseMapper.warehouseFromDto(warehouseDto, id);
            warehouseService.update(warehouse, user);

            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("addressId", "duplicate_warehouse", e.getMessage());
            return "warehouse-update";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("addressId", "entity-not-found", e.getMessage());
            return "warehouse-new";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteWarehouse(@PathVariable int id, Model model,HttpSession session) {
        //todo Catch if warehouse is already in use
        User user;
        try {
            user = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            return "denied";
        }

        try {
            warehouseService.delete(id, user);

            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/search")
    public String searchForWarehouse(@ModelAttribute SearchTermDto searchTermDto,
                                    Model model) {

        var search = warehouseService.search(
                Optional.ofNullable(searchTermDto.getSearchTerm()),
                Optional.ofNullable(searchTermDto.getSort()));

        model.addAttribute("warehouses", search);

        return "warehouses";
    }
}
