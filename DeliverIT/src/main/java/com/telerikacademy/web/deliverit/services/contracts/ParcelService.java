package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;

import java.util.List;
import java.util.Optional;

public interface ParcelService {
    List<Parcel> getAll();

    Parcel getById(int id, User loggedUser);

    void create(Parcel parcel, User loggedUser);

    void update(Parcel parcel, User loggedUser);

    void delete(int id, User loggedUser);

    List<Parcel> filter(Optional<Double> minWeight,
                        Optional<Double> maxWeight,
                        Optional<Integer> customerId,
                        Optional<Integer> categoryId,
                        Optional<Integer> warehouseId,
                        Optional<String> destination,
                        Optional<Integer> shipmentStatusId,
                        Optional<String> customerKeyWord,
                        Optional<String> sort);

    List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate);

    Shipment getShipmentByParcelId(int id, User loggedUser);

    void removeParcelFromShipment(Parcel parcel, User loggedUser);

}
