package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.AdminFailureException;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.SearchTermDto;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/employees")
public class EmployeeMvcController {

    private final EmployeeService employeeService;
    private final CountryService countryService;
    private final CityService cityService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeMvcController(EmployeeService employeeService,
                                 CountryService countryService,
                                 CityService cityService,
                                 AuthenticationHelper authenticationHelper) {
        this.employeeService = employeeService;
        this.countryService = countryService;
        this.cityService = cityService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @GetMapping
    public String showAllEmployees(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
        model.addAttribute("employees", employeeService.getAll(user));
        model.addAttribute("searchTermDto", new SearchTermDto());
        return "employees";
    }

    @GetMapping("/{id}")
    public String showSingleEmployee(@PathVariable int id,
                                     Model model,
                                     HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            return "denied";
        }
        try {
            User employee = employeeService.getById(id, user);
            model.addAttribute("employee", employee);
            return "employee";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/search")
    public String searchForEmployee(@ModelAttribute SearchTermDto searchTermDto,
                                    HttpSession session,
                                    Model model) {

        try {
            authenticationHelper.tryGetAdmin(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/auth/login";
        } catch (AdminFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "denied";
        }
        var search = employeeService.search(
                Optional.ofNullable(searchTermDto.getSearchTerm()),
                Optional.ofNullable(searchTermDto.getSort()));

        model.addAttribute("employees", search);

        return "employees";
    }
}
