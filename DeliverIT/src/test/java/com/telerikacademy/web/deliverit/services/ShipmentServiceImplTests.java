package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

import static com.telerikacademy.web.deliverit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceImplTests {

    @Mock
    ShipmentRepository mockShipmentRepository;

    @InjectMocks
    ShipmentServiceImpl mockShipmentService;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockShipmentRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockShipmentService.getAll();

        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository() {
        Shipment mockShipment = createMockShipment();

        Mockito.when(mockShipmentRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        Shipment result = mockShipmentService.getById(mockShipment.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockShipment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockShipment.getEmployee(), result.getEmployee()),
                () -> Assertions.assertEquals(mockShipment.getArrivalWarehouse(), result.getArrivalWarehouse()),
                () -> Assertions.assertEquals(mockShipment.getDepartureWarehouse(), result.getDepartureWarehouse()),
                () -> Assertions.assertEquals(mockShipment.getStatus(), result.getStatus())
        );
    }

    @Test
    public void create_should_callRepository_when_userIsAdmin() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockAdmin();

        mockShipmentService.create(mockShipment, mockUser);

        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).create(mockShipment);
    }

    @Test
    public void create_should_throwException_when_userIsNotAdmin() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockShipmentService.create(mockShipment, mockUser));
    }

    @Test
    public void update_should_callRepository_when_userIsAdminAndUpdateInfoIsCorrect() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockAdmin();

        mockShipmentService.update(mockShipment, mockUser);

        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).update(mockShipment);
    }

    @Test
    public void update_should_throwException_when_userIsNotAdminAndUpdateInfoIsCorrect() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockShipmentService.update(mockShipment, mockUser));
    }

    @Test
    public void update_should_throwException_when_userIsAdminAndUpdateWarehousesAreTheSame() {
        Shipment mockShipment = createMockShipment();
        mockShipment.setDepartureWarehouse(createMockWarehouse());

        User mockUser = createMockAdmin();

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> mockShipmentService.update(mockShipment, mockUser));
    }

    @Test
    public void update_should_throwException_when_userIsAdminAndUpdateDatesAreNotCorrect() throws ParseException {
        Shipment mockShipment = createMockShipment();
        LocalDate departureDate = LocalDate.parse("2021-08-02");
        LocalDate arrivalDate = LocalDate.parse("2021-08-01");

        mockShipment.setArrivalDate(arrivalDate);
        mockShipment.setDepartureDate(departureDate);

        User mockUser = createMockAdmin();

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> mockShipmentService.update(mockShipment, mockUser));
    }

    @Test
    public void update_should_changeShipmentStatus_when_updateDepartureDate() throws ParseException {
        Shipment mockShipment = createMockShipment();
        LocalDate departureDate = LocalDate.parse("2021-08-01");

        mockShipment.setDepartureDate(departureDate);

        User mockUser = createMockAdmin();

        mockShipmentService.update(mockShipment, mockUser);

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockShipment.getStatus(), Status.ON_THE_WAY)
        );
    }

    @Test
    public void update_should_changeShipmentStatus_when_updateArrivalDate() throws ParseException {
        Shipment mockShipment = createMockShipment();
        LocalDate departureDate = LocalDate.parse("2021-08-01");
        LocalDate arrivalDate = LocalDate.parse("2021-08-02");

        mockShipment.setDepartureDate(departureDate);
        mockShipment.setArrivalDate(arrivalDate);

        User mockUser = createMockAdmin();

        mockShipmentService.update(mockShipment, mockUser);

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockShipment.getStatus(), Status.COMPLETED)
        );
    }

//    @Test
//    public void addParcel_should_callRepository_when_userIsAdmin() {
//        Shipment mockShipment = createMockShipment();
//        User mockUser = createMockAdmin();
//        Parcel mockParcel = createMockParcel();
//
//        mockShipmentService.addParcel(mockShipment, mockParcel, mockUser);
//
//        Mockito.verify(mockShipmentRepository,
//                Mockito.times(1)).addParcel(mockShipment, mockParcel);
//    }
//
//    @Test
//    public void addParcel_should_throwException_when_userIsNotAdmin() {
//        Shipment mockShipment = createMockShipment();
//        User mockUser = createMockUser();
//        Parcel mockParcel = createMockParcel();
//
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> mockShipmentService.addParcel(mockShipment, mockParcel, mockUser));
//    }

    @Test
    public void delete_should_callRepository_whenUserIsAdmin() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockAdmin();

        mockShipmentService.delete(mockShipment.getId(), mockUser);

        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).delete(mockShipment.getId());
    }

    @Test
    public void delete_should_throwException_whenUserIsNotAdmin() {
        Shipment mockShipment = createMockShipment();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockShipmentService.delete(mockShipment.getId(), mockUser));
    }

    @Test
    public void filter_should_callRepository() {
        mockShipmentService.filter(
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        Mockito.verify(mockShipmentRepository,
                Mockito.times(1)).filter(
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

}
