package com.telerikacademy.web.deliverit.models.mappers;

import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.ShipmentArrivalDateDto;
import com.telerikacademy.web.deliverit.models.dtos.ShipmentDepartureDateDto;
import com.telerikacademy.web.deliverit.models.dtos.ShipmentDto;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;


@Component
public class ShipmentMapper {

    public static final String DATE_ERROR_MSG = "Date is not in the required format.(yyyy-MM-dd)";
    private final ShipmentRepository shipmentRepository;
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public ShipmentMapper(ShipmentRepository shipmentRepository , WarehouseRepository warehouseRepository) {
        this.shipmentRepository = shipmentRepository;
        this.warehouseRepository = warehouseRepository;
    }

    public Shipment shipmentFromDto(ShipmentDto shipmentDto) {
        Shipment shipment = new Shipment();
        shipmentDtoToObject(shipmentDto, shipment);

        return shipment;
    }


    public Shipment shipmentFromDto(ShipmentDto shipmentDto, int id) {
        Shipment shipment = shipmentRepository.getById(id);
        shipmentDtoToObject(shipmentDto, shipment);

        return shipment;
    }

    public Shipment setArrivalDate(ShipmentArrivalDateDto shipmentArrivalDateDto, int id) {
        Shipment shipment = shipmentRepository.getById(id);

        LocalDate arrDate = null;
        try {
            arrDate = LocalDate.parse(shipmentArrivalDateDto.getArrivalDate());
        } catch (DateTimeParseException e) {
            throw new UnsupportedOperationException(DATE_ERROR_MSG);
        }
        shipment.setArrivalDate(arrDate);

        return shipment;
    }

    public Shipment setDepartureDate(ShipmentDepartureDateDto shipmentDepartureDateDto, int id) {
        Shipment shipment = shipmentRepository.getById(id);

        LocalDate depDate = null;
        try {
            depDate = LocalDate.parse(shipmentDepartureDateDto.getDepartureDate());
        } catch (DateTimeParseException e) {
            throw new UnsupportedOperationException(DATE_ERROR_MSG);
        }
        shipment.setDepartureDate(depDate);

        return shipment;
    }

    private void shipmentDtoToObject(ShipmentDto shipmentDto, Shipment shipment)  {
        Warehouse departureWarehouse = warehouseRepository.getById(shipmentDto.getDepartureWarehouseId());
        Warehouse arrivalWarehouse = warehouseRepository.getById(shipmentDto.getArrivalWarehouseId());

/*        if(!shipmentDto.getDepartureDate().equals("")) {
            shipment.setDepartureDate(parseStringToDate(shipmentDto.getDepartureDate()));
        }*/

        shipment.setDepartureWarehouse(departureWarehouse);
        shipment.setArrivalWarehouse(arrivalWarehouse);
        shipment.setStatus(Status.values()[0]);
    }

    private LocalDate parseStringToDate(String stringDate) {
        try {
            return LocalDate.parse(stringDate);
        } catch (DateTimeParseException e) {
            throw new UnsupportedOperationException(DATE_ERROR_MSG);
        }

    }
}
