package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.LoginDto;
import com.telerikacademy.web.deliverit.models.dtos.RegisterUserDto;
import com.telerikacademy.web.deliverit.models.mappers.AddressMapper;
import com.telerikacademy.web.deliverit.models.mappers.UserMapper;
import com.telerikacademy.web.deliverit.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final CustomerService customerService;
    private final AddressService addressService;
    private final AddressMapper addressMapper;
    private final CountryService countryService;
    private final CityService cityService;

    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper,
                                    CustomerService customerService,
                                    AddressService addressService,
                                    AddressMapper addressMapper,
                                    CountryService countryService,
                                    CityService cityService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.customerService = customerService;
        this.addressService = addressService;
        this.addressMapper = addressMapper;
        this.countryService = countryService;
        this.cityService = cityService;
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getEmail(), login.getPassword());
            User user = userService.getByEmail(login.getEmail());
            session.setAttribute("currentUser", login.getEmail());
            session.setAttribute("firstName", user.getFirstName());
            session.setAttribute("lastName", user.getLastName());
            session.setAttribute("userId", user.getId());
            session.setAttribute("userRole", user.getRole());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        session.removeAttribute("firstName");
        session.removeAttribute("lastName");
        session.removeAttribute("userId");
        session.removeAttribute("userRole");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterUserDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterUserDto register,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Passwords do not match");
            return "register";
        }

        try {
            Address address = addressMapper.addressFromDto(register.getAddressDto());
            addressService.create(address);

            Address userAddress = addressService.getByStreet(address.getStreet());

            User user = userMapper.userFromDto(register);
            user.setAddress(userAddress);

            customerService.create(user);
            return "redirect:/";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "register";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("addressId", "entity-not-found", e.getMessage());
            return "register";
        }
    }
}
