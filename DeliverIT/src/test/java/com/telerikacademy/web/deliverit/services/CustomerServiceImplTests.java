package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.Helpers.createMockAdmin;
import static com.telerikacademy.web.deliverit.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTests {

    @Mock
    CustomerRepository mockCustomerRepository;

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    CustomerServiceImpl mockCustomerService;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockCustomerRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockCustomerService.getAll();

        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockCustomerRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = mockCustomerService.getById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getAddress(), result.getAddress()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole())
        );
    }

    @Test
    public void create_should_callRepository_when_thereIsNoSuchUser() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        mockCustomerService.create(mockUser);

        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).create(mockUser);
    }

    @Test
    public void create_should_throw_when_userWithSameNameExists() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockCustomerService.create(mockUser));
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingUser() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        mockCustomerService.update(mockUser, mockUser);

        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).update(mockUser);

    }

    @Test
    public void update_should_throwException_when_userIsNotCreatorOrAdmin() {
        User mockUser = createMockUser();
        User otherMockUser = createMockUser();
        otherMockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockCustomerService.update(mockUser, otherMockUser));

    }

    @Test
    public void update_should_throwException_when_thereIsSuchUser() {
        User mockUser = createMockUser();
        User otherMockUser = createMockAdmin();

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockCustomerService.update(mockUser, otherMockUser));

    }

    @Test
    public void delete_should_callRepository_when_thereIsAdmin() {
        User mockUser = createMockUser();
        User otherMockUser = createMockAdmin();
        otherMockUser.setId(2);

        mockCustomerService.delete(mockUser.getId(), otherMockUser);

        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).delete(mockUser.getId());

    }

    @Test
    public void delete_should_throwException_when_userIsNotCreatorOrAdmin() {
        User mockUser = createMockUser();
        User otherMockUser = createMockUser();
        otherMockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockCustomerService.delete(mockUser.getId(), otherMockUser));

    }

    @Test
    public void search_should_callRepository() {
        Mockito.when(mockCustomerRepository.search(
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty()))
                .thenReturn(new ArrayList<>());

        mockCustomerService.search(
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).search(
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @Test
    public void showParcelsByCustomerId_should_callRepository_whenUserIsAdmin() {
        User mockUser = createMockUser();
        User otherMockUser = createMockAdmin();
        otherMockUser.setId(2);

        mockCustomerService.showParcelsByCustomerId(mockUser.getId(), otherMockUser);

        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).showParcelsByCustomerId(mockUser.getId());
    }

    @Test
    public void showParcelsByCustomerId_should_throwException_whenUserIsNotCreatorOrAdmin() {
        User mockUser = createMockUser();
        User otherMockUser = createMockUser();
        otherMockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockCustomerService.showParcelsByCustomerId(mockUser.getId(), otherMockUser));
    }

}
