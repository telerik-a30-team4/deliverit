package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAll();

    User getByEmail(String email);
}
