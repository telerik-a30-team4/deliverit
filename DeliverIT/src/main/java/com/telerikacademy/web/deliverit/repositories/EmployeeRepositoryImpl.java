package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.repositories.contracts.EmployeeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where u.role=0", User.class);

            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User employee = session.get(User.class, id);

            if (employee == null || !employee.getRole().equals(Role.ADMIN)) {
                throw new EntityNotFoundException("Employee", "id", String.valueOf(id));
            }

            return employee;
        }
    }

    @Override
    public List<User> search(Optional<String> searchTerm, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from User where role=0";

            if (searchTerm.isPresent()) queryString += " and concat(firstName, ' ', lastName, ' ', email) like ?1";
            if (sort.isPresent()) queryString += " order by " + sort.get();

            Query<User> query = session.createQuery(queryString, User.class);

            if (searchTerm.isPresent()) query.setParameter(1, "%" + searchTerm.get() + "%");

            return query.list();
        }
    }

}
