package com.telerikacademy.web.deliverit.models.mappers;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.RegisterUserDto;
import com.telerikacademy.web.deliverit.models.dtos.UserDto;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final CustomerRepository customerRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public UserMapper(CustomerRepository customerRepository, AddressRepository addressRepository) {
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
    }

    public User userFromDto(UserDto userDto) {
        User user = new User();
        userDtoToObject(userDto, user);

        return user;
    }

    public User userFromDto(UserDto userDto, int id) {
        User user = customerRepository.getById(id);
        userDtoToObject(userDto, user);

        return user;
    }



    private void userDtoToObject(UserDto userDto, User user) {
        Address address = addressRepository.getById(userDto.getAddressId());

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setAddress(address);
    }

    public User userFromDto(RegisterUserDto registerUserDto) {
        User user = new User();
        user.setEmail(registerUserDto.getEmail());
        user.setFirstName(registerUserDto.getFirstName());
        user.setLastName(registerUserDto.getLastName());
        user.setRole(Role.USER);
        user.setPassword(registerUserDto.getPassword());

        return user;
    }
}
