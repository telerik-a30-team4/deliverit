package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    public static final String ADMIN_REQUIRE_MSG = "Only admins can create, modify or delete warehouses";
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    @Override
    public List<Warehouse> getAll() {
        return warehouseRepository.getAll();
    }

    @Override
    public Warehouse getById(int id) {
        return warehouseRepository.getById(id);
    }

    @Override
    public List<Warehouse> getByCityId(int cityId) {
        return warehouseRepository.getByCityId(cityId);
    }

    @Override
    public void create(Warehouse warehouse, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_REQUIRE_MSG);
        }

        List<Warehouse> warehouses = warehouseRepository.getAll();

        if (warehouses.stream().anyMatch(w -> w.getAddress().getCity().getId() == warehouse.getAddress().getCity().getId())) {
            throw new DuplicateEntityException("Warehouse", "CityId", String.valueOf(warehouse.getAddress().getCity().getId()));
        }

        warehouseRepository.create(warehouse);
    }

    @Override
    public void update(Warehouse warehouse, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_REQUIRE_MSG);
        }

        List<Warehouse> warehouses = warehouseRepository.getAll();

        if (warehouses.stream().anyMatch(w -> w.getAddress().getCity().getId() == warehouse.getAddress().getCity().getId())) {
            throw new DuplicateEntityException("Warehouse", "CityId", String.valueOf(warehouse.getAddress().getCity().getId()));
        }

        warehouseRepository.update(warehouse);
    }

    @Override
    public void delete(int id, User loggedUser) {
        if (!loggedUser.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_REQUIRE_MSG);
        }
        warehouseRepository.delete(id);
    }

    @Override
    public List<Warehouse> search(Optional<String> searchTerm,
                             Optional<String> sort) {
        return warehouseRepository.search(searchTerm,sort);
    }

}
