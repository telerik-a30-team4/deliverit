package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;

import java.util.List;
import java.util.Optional;

public interface WarehouseService {
    List<Warehouse> getAll();

    Warehouse getById(int id);

    List<Warehouse> getByCityId(int cityId);

    void create(Warehouse warehouse, User loggedUser);

    void update(Warehouse warehouse, User loggedUser);

    void delete(int id, User loggedUser);

    List<Warehouse> search(Optional<String> searchTerm, Optional<String> sort);
}
