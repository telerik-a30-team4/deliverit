package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Address;

import java.util.List;

public interface AddressService {
    List<Address> getAll();

    void create(Address address);

    Address getByStreet(String street);
}
