package com.telerikacademy.web.deliverit.models.enums;

public enum Status {

    PREPARING,
    ON_THE_WAY,
    COMPLETED

}
