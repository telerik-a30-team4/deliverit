package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ParcelRepositoryImpl implements ParcelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Parcel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel", Parcel.class);
            return query.list();
        }
    }

    @Override
    public Parcel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Parcel parcel = session.get(Parcel.class, id);
            if (parcel == null) {
                throw new EntityNotFoundException("Parcel", "id", String.valueOf(id));
            }
            return parcel;
        }
    }

    @Override
    public void create(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcel);
        }
    }

    @Override
    public void update(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Parcel parcel = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcel);
            session.getTransaction().commit();
        }
    }

    //TODO vsichko da minava prez Shipment
    @Override
    public List<Parcel> filter(Optional<Double> minWeight,
                               Optional<Double> maxWeight,
                               Optional<Integer> customerId,
                               Optional<Integer> categoryId,
                               Optional<Integer> warehouseId,
                               Optional<String> destination,
                               Optional<Integer> shipmentStatusId,
                               Optional<String> customerKeyWord,
                               Optional<String> sort) {

        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Parcel where 1=1";

            //FILTER BY WAREHOUSE
            if (warehouseId.isPresent() && destination.isEmpty()) {
                queryString += " AND shipment.arrivalWarehouse.id = :warehouseId OR shipment.departureWarehouse.id = :warehouseId";
            }
            if (warehouseId.isPresent() && destination.isPresent()) {
                if (destination.get().equals("arr")) {
                    queryString += " AND shipment.arrivalWarehouse.id = :warehouseId";
                } else if (destination.get().equals("dep")) {
                    queryString += " AND shipment.departureWarehouse.id = :warehouseId";
                }
            }

            //FILTER BY CUSTOMER
            if (customerId.isPresent()) queryString += " AND customer.id = :customerId";

            //FILTER BY Shipment status
            if (shipmentStatusId.isPresent()) {
                queryString += " AND shipment.status = :shipmentStatus";
            }

            //FILTER BY WEIGHT
            if (maxWeight.isPresent()) queryString += " AND weight < :maxWeight";
            if (minWeight.isPresent()) queryString += " AND weight > :minWeight";

            //FILTER BY CATEGORY
            if (categoryId.isPresent()) queryString += " AND category.id = :category";

            //FILTER BY CUSTOMER KEY WORD
            if (customerKeyWord.isPresent() && !customerKeyWord.get().isEmpty()) {
                queryString += " AND concat(customer.email, ' ', customer.firstName, ' ', customer.lastName) like ?1";
            }

            if (sort.isPresent()) queryString += " order by " + sort.get();

            Query<Parcel> query = session.createQuery(queryString, Parcel.class);

            if (warehouseId.isPresent()) query.setParameter("warehouseId", warehouseId.get());
            if (customerId.isPresent()) query.setParameter("customerId", customerId.get());
            if (maxWeight.isPresent()) query.setParameter("maxWeight", maxWeight.get());
            if (minWeight.isPresent()) query.setParameter("minWeight", minWeight.get());
            if (categoryId.isPresent()) query.setParameter("category", categoryId.get());
            if (shipmentStatusId.isPresent())
                query.setParameter("shipmentStatus", Status.values()[shipmentStatusId.get()]);
            if (customerKeyWord.isPresent() && !customerKeyWord.get().isEmpty())
                query.setParameter(1, "%" + customerKeyWord.get() + "%");

            return query.list();

        } catch (ArrayIndexOutOfBoundsException e) {
            throw new EntityNotFoundException("Attribute", "such", "id");
        }
    }

    @Override
    public List<Parcel> sort(Optional<String> weight,
                             Optional<String> arrivalDate) {

        try (Session session = sessionFactory.openSession()) {

            String queryString = "from Parcel where 1=1";
            if (weight.isPresent() && arrivalDate.isEmpty()) {
                queryString += " order by weight";
            }

            if (weight.isEmpty() && arrivalDate.isPresent()) {
                queryString += " AND shipment.arrivalDate != null order by shipment.arrivalDate";
            }

            if (weight.isPresent() && arrivalDate.isPresent()) {
                queryString += " AND shipment.arrivalDate != null order by weight, shipment.arrivalDate";
            }

            Query<Parcel> query = session.createQuery(queryString, Parcel.class);
            return query.list();

        }
    }

    //ToDo - remove this method
    @Override
    public Shipment getShipmentByParcelId(int id) {
        return getById(id).getShipment();
    }

    //ToDo - remove this method
    @Override
    public void removeParcelFromShipment(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            parcel.setShipment(null);

            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        } catch (IndexOutOfBoundsException e) {
            throw new EntityNotFoundException("Parcel is not in shipment");
        }
    }

}