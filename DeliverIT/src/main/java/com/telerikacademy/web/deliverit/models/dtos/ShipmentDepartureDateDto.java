package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotNull;

public class ShipmentDepartureDateDto {

    @NotNull
    private String departureDate;

    public ShipmentDepartureDateDto() {
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }
}
