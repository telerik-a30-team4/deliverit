package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment", Shipment.class);

            return query.list();
        }
    }

    @Override
    public Shipment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", "id", String.valueOf(id));
            }
            return shipment;
        }
    }

    @Override
    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipment);
        }
    }

    @Override
    public void update(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Shipment shipment = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipment> filter(Optional<Integer> warehouseId,
                                 Optional<String> destination,
                                 Optional<Integer> customerId,
                                 Optional<String> employeeKeyWord,
                                 Optional<Integer> statusId,
                                 Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Shipment where 1=1";

            // FILTER BY WAREHOUSE
            if (warehouseId.isPresent() && destination.isEmpty()) {
                queryString += " AND (departureWarehouse.id = :warehouseId OR arrivalWarehouse.id = :warehouseId)";
            } else if (warehouseId.isPresent() && destination.get().equals("arr")) {
                queryString += " AND arrivalWarehouse.id = :warehouseId";
            } else if (warehouseId.isPresent() && destination.get().equals("dep")) {
                queryString += " AND departureWarehouse.id = :warehouseId";
            }
            //FILTER BY CUSTOMER
            if (customerId.isPresent()) {
                queryString = "select p.shipment from Parcel p where p.customer.id= :customerId";
            }

            //FILTER BY EMPLOYEE KEY WORD
            if (employeeKeyWord.isPresent() && !employeeKeyWord.get().equals("")) {
                queryString += " AND concat(employee.email, ' ', employee.firstName, ' ', employee.lastName) like ?1";
                //queryString += " AND employee.email like ?1";
            }

            // FILTER BY STATUS
            if (statusId.isPresent()) {
                queryString += " AND status = :status";
            }

            if (sort.isPresent()) queryString += " order by " + sort.get();

            Query<Shipment> query = session.createQuery(queryString, Shipment.class);

            if (warehouseId.isPresent()) query.setParameter("warehouseId", warehouseId.get());
            if (customerId.isPresent()) query.setParameter("customerId", customerId.get());
            if (employeeKeyWord.isPresent() && !employeeKeyWord.get().equals("")) query.setParameter(1, "%" + employeeKeyWord.get() + "%");
            if (statusId.isPresent()) query.setParameter("status", Status.values()[statusId.get()]);

            return query.list();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new EntityNotFoundException("Attribute", "such", "id");
        }
    }

    @Override
    public List<Parcel> getShipmentParcels(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where shipment.id = :id", Parcel.class);
            query.setParameter("id", id);

            return query.list();
        }
    }

    @Override
    public void addParcel(Shipment shipment, Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            parcel.setShipment(shipment);

            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipment> search(String searchTerm) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Shipment where employee.email like ?1";

            Query<Shipment> query = session.createQuery(queryString, Shipment.class);
            query.setParameter(1, "%" + searchTerm + "%");

            return query.list();
        }
    }

}
