package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.dtos.DisplayInfoParcelDto;
import com.telerikacademy.web.deliverit.models.dtos.ParcelDto;
import com.telerikacademy.web.deliverit.models.dtos.ParcelPickUpDto;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.models.mappers.ParcelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/parcels")
public class ParcelController {

    private final ParcelService parcelService;
    private final ParcelMapper parcelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelController(ParcelService parcelService, ParcelMapper parcelMapper, AuthenticationHelper authenticationHelper) {
        this.parcelService = parcelService;
        this.parcelMapper = parcelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<DisplayInfoParcelDto> getAllParcels(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);

        return parcelService.getAll()
                .stream()
                .map(parcel -> new DisplayInfoParcelDto(parcel.getId(), parcel.getWeight(), parcel.getCategory().getId(), parcel.getCustomer().getId(), parcel.isPickUpFromWarehouse()))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Parcel findParcelById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            return parcelService.getById(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/shipment")
    public Shipment getShipmentByParcelId(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            return parcelService.getShipmentByParcelId(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<DisplayInfoParcelDto> filter(@RequestHeader HttpHeaders headers,
                                             @RequestParam(required = false) Optional<Double> minWeight,
                                             @RequestParam(required = false) Optional<Double> maxWeight,
                                             @RequestParam(required = false) Optional<Integer> customerId,
                                             @RequestParam(required = false) Optional<Integer> categoryId,
                                             @RequestParam(required = false) Optional<Integer> warehouseId,
                                             @RequestParam(required = false) Optional<String> destination,
                                             @RequestParam(required = false) Optional<Integer> shipmentStatusId,
                                             @RequestParam(required = false) Optional<String> customerKeyWord,
                                             @RequestParam(required = false) Optional<String> sort) {

        authenticationHelper.tryGetUser(headers);

        try {
            return parcelService.filter(minWeight, maxWeight, customerId, categoryId, warehouseId, destination, shipmentStatusId, customerKeyWord, sort)
                    .stream()
                    .map(parcel -> new DisplayInfoParcelDto(parcel.getId(), parcel.getWeight(), parcel.getCategory().getId(), parcel.getCustomer().getId(), parcel.isPickUpFromWarehouse()))
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sort")
    public List<DisplayInfoParcelDto> sort(@RequestHeader HttpHeaders headers,
                                           @RequestParam(required = false) Optional<String> weight,
                                           @RequestParam(required = false) Optional<String> arrivalDate) {

        authenticationHelper.tryGetUser(headers);

        return parcelService.sort(weight, arrivalDate)
                .stream()
                .map(parcel -> new DisplayInfoParcelDto(parcel.getId(), parcel.getWeight(), parcel.getCategory().getId(), parcel.getCustomer().getId(), parcel.isPickUpFromWarehouse()))
                .collect(Collectors.toList());
    }

    @PostMapping
    public Parcel createParcel(@RequestHeader HttpHeaders headers,
                               @RequestBody @Valid ParcelDto parceldto) {

        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Parcel parcelToCreate = parcelMapper.parcelFromDto(parceldto);
            parcelService.create(parcelToCreate, loggedUser);
            return parcelToCreate;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Parcel updateParcel(@RequestHeader HttpHeaders headers,
                               @PathVariable int id,
                               @RequestBody @Valid ParcelDto parceldto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Parcel parcelToUpdate = parcelMapper.parcelFromDto(parceldto, id);
            parcelService.update(parcelToUpdate, loggedUser);
            return parcelToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/pickup")
    public Parcel updateParcelPickUp(@RequestHeader HttpHeaders headers,
                                     @PathVariable int id,
                                     @RequestBody @Valid ParcelPickUpDto parcelPickUpDto) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Parcel parcelToUpdate = parcelMapper.setParcelPickUp(parcelPickUpDto, id);
            parcelService.update(parcelToUpdate, loggedUser);
            return parcelToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteParcel(@RequestHeader HttpHeaders headers,
                             @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            parcelService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{parcelId}/from-shipment")
    public void removeParcelFromShipment(@RequestHeader HttpHeaders headers,
                                         @PathVariable int parcelId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);

            Parcel parcel = parcelService.getById(parcelId, loggedUser);

            parcelService.removeParcelFromShipment(parcel, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
