package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.web.deliverit.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class AddressServiceImplTests {
    @Mock
    AddressRepository mockAddressRepository;

    @InjectMocks
    AddressServiceImpl mockAddressService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockAddressRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockAddressService.getAll();

        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void create_should_callRepository() {
        Address address = createMockAddress();

        mockAddressService.create(address);

        Mockito.verify(mockAddressRepository,
                Mockito.times(1)).create(address);
    }
}
