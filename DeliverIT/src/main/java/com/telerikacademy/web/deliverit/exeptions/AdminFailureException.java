package com.telerikacademy.web.deliverit.exeptions;

public class AdminFailureException extends RuntimeException {
    public AdminFailureException(String message) {
        super(message);
    }
}
