package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.repositories.contracts.CountryRepository;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository mockCountryRepository;

    @InjectMocks
    CountryServiceImpl mockCountryService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockCountryRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockCountryService.getAll();

        Mockito.verify(mockCountryRepository,
                Mockito.times(1)).getAll();
    }
}
