package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeController {

    private final CustomerService customerService;

    @Autowired
    public HomeController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/contact")
    public String showContactPage() {
        return "contact";
    }

    //TODO How to make it global?
    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("CustomersCount")
    public String customersCount(){
        return String.valueOf(customerService.getAll().size());
    }
}
