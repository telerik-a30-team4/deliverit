package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ShipmentDto {

    private String departureDate;

    private String arrivalDate;

    @NotNull
    @Positive
    private int departureWarehouseId;

    @NotNull
    @Positive
    private int arrivalWarehouseId;

    public ShipmentDto() {
    }

    public ShipmentDto(String departureDate, String arrivalDate, int departureWarehouseId, int arrivalWarehouseId) {
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.departureWarehouseId = departureWarehouseId;
        this.arrivalWarehouseId = arrivalWarehouseId;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getDepartureWarehouseId() {
        return departureWarehouseId;
    }

    public void setDepartureWarehouseId(int departureWarehouseId) {
        this.departureWarehouseId = departureWarehouseId;
    }

    public int getArrivalWarehouseId() {
        return arrivalWarehouseId;
    }

    public void setArrivalWarehouseId(int arrivalWarehouseId) {
        this.arrivalWarehouseId = arrivalWarehouseId;
    }
}
