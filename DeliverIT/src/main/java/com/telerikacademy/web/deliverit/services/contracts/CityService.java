package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.City;

import java.util.List;

public interface CityService {
    List<City> getAll();
}
