package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getByEmail(String email);
}
