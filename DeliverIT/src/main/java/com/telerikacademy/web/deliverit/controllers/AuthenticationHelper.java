package com.telerikacademy.web.deliverit.controllers;

import com.telerikacademy.web.deliverit.exeptions.AdminFailureException;
import com.telerikacademy.web.deliverit.exeptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.enums.Role;
import com.telerikacademy.web.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_EMAIL = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MSG = "Wrong username or password!";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_EMAIL)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested resource requires authentication.");
        }

        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_EMAIL);
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid email.");
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException("Please login to continue.");
        }

        return userService.getByEmail(currentUser);
    }

    public User tryGetAdmin(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException("Please login to continue.");
        }

         User user = userService.getByEmail(currentUser);

        if (!user.getRole().equals(Role.ADMIN)) {
            throw new AdminFailureException("You should be admin.");
        }

        return user;
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByEmail(username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MSG);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MSG);
        }
    }




}
