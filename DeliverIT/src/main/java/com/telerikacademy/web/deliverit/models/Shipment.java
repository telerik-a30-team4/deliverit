package com.telerikacademy.web.deliverit.models;

import com.telerikacademy.web.deliverit.models.enums.Status;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "shipments")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_id")
    private int id;

    @Column(name = "departure_date")
    private LocalDate departureDate;

    @Column(name = "arrival_date")
    private LocalDate arrivalDate;

    @Column(name = "status_id")
    @Enumerated
    private Status status;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private User employee;

    @ManyToOne
    @JoinColumn(name = "departure_warehouse_id")
    private Warehouse departureWarehouse;

    @ManyToOne
    @JoinColumn(name = "arrival_warehouse_id")
    private Warehouse arrivalWarehouse;

    public Shipment() {
    }

    public Shipment(int id, LocalDate departureDate, LocalDate arrivalDate, Status status) {
        this.id = id;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }

    public Warehouse getDepartureWarehouse() {
        return departureWarehouse;
    }

    public void setDepartureWarehouse(Warehouse departureWarehouse) {
        this.departureWarehouse = departureWarehouse;
    }

    public Warehouse getArrivalWarehouse() {
        return arrivalWarehouse;
    }

    public void setArrivalWarehouse(Warehouse arrivalWarehouse) {
        this.arrivalWarehouse = arrivalWarehouse;
    }

    public boolean isModifiable() {
        return status.equals(Status.PREPARING);
    }

    public boolean isCompleted() {
        return status.equals(Status.COMPLETED);
    }

}
