package com.telerikacademy.web.deliverit.models.dtos;

public class ShipmentSearchDto {

    private Integer warehouseId;
    private String destination;
    private String employeeKeyWord;
    private Integer statusId;
    private String sort;

    public ShipmentSearchDto() {
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEmployeeKeyWord() {
        return employeeKeyWord;
    }

    public void setEmployeeKeyWord(String employeeKeyWord) {
        this.employeeKeyWord = employeeKeyWord;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
