package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTests {

    @Mock
    CityRepository mockCityRepository;

    @InjectMocks
    CityServiceImpl mockCityService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockCityRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockCityService.getAll();

        Mockito.verify(mockCityRepository,
                Mockito.times(1)).getAll();
    }
}
