package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.Helpers;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.deliverit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceImplTests {

    @Mock
    WarehouseRepository mockWarehouseRepository;

    @InjectMocks
    WarehouseServiceImpl mockWarehouseService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockWarehouseRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockWarehouseService.getAll();

        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    void getById_should_callRepository() {
        Warehouse mockWarehouse = createMockWarehouse();

        Mockito.when(mockWarehouseRepository.getById(mockWarehouse.getId()))
                .thenReturn(mockWarehouse);

        Warehouse result = mockWarehouseService.getById(mockWarehouse.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWarehouse.getId(), result.getId()),
                () -> Assertions.assertEquals(mockWarehouse.getAddress(), result.getAddress())
        );
    }

    @Test
    void create_should_callRepository() {
        Warehouse warehouse = createMockWarehouse();
        User mockAdmin = createMockAdmin();

        mockWarehouseService.create(warehouse, mockAdmin);

        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).create(warehouse);
    }

    @Test
    void create_should_throw_when_noAdmin() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWarehouseService.create(mockWarehouse, mockUser));
    }

    @Test
    void create_should_throw_when_duplicateAddress() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockAdmin = createMockAdmin();

        List<Warehouse> warehouses = new ArrayList<>();
        warehouses.add(mockWarehouse);

        Mockito.when(mockWarehouseRepository.getAll())
                .thenReturn(warehouses);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockWarehouseService.create(mockWarehouse,mockAdmin));
    }

    @Test
    void update_should_callRepository_when_adminIsLogged() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockAdmin = createMockAdmin();

        mockWarehouseService.update(mockWarehouse, mockAdmin);

        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).update(mockWarehouse);
    }

    @Test
    void update_should_throw_when_userIsLogged() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWarehouseService.update(mockWarehouse,mockUser));

    }

    @Test
    void update_should_throw_when_addressIsDuplicated() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockAdmin = createMockAdmin();

        List<Warehouse> warehouses = new ArrayList<>();
        warehouses.add(mockWarehouse);

        Mockito.when(mockWarehouseRepository.getAll())
                .thenReturn(warehouses);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockWarehouseService.update(mockWarehouse,mockAdmin));
    }

    @Test
    void delete_should_callRepository_when_adminIsLogged() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockAdmin = createMockAdmin();

        mockWarehouseService.delete(mockWarehouse.getId(), mockAdmin);

        Mockito.verify(mockWarehouseRepository,
                Mockito.times(1)).delete(mockWarehouse.getId());
    }

    @Test
    void delete_should_throw_when_userIsLogged() {
        Warehouse mockWarehouse = createMockWarehouse();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWarehouseService.delete(mockWarehouse.getId(), mockUser));
    }

}
